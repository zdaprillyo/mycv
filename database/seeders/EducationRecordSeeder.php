<?php

namespace Database\Seeders;

use App\Models\EducationRecord;
use Illuminate\Database\Seeder;

class EducationRecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = [
            ['institution_name' => 'SMKN 1 Sampit','start_at' => '2014-04-01','end_at'=>'2017-06-01','desc' => 'SMK', 'educational_level_id' => 3],
            ['institution_name' => 'Universitas Surabaya','start_at' => '2017-08-01','end_at'=>'2021-03-01','desc' => 'Gelar S.Kom', 'educational_level_id' => 8],
        ];
        foreach ($records as $dt) {
            EducationRecord::create($dt);
        }
    }
}
