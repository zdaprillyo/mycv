<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['name' => 'Fullstack Developer','is_developer' => 1, 'desc' => 'Adalah pengembang perangkat lunak utama yang memiliki kemampuan di berbagai pokok keahlian untuk membuat produk perangkat lunak'],
            ['name' => 'Main Fullstack Developer','is_developer' => 1, 'desc' => 'Adalah pengembang perangkat lunak utama yang memiliki kemampuan di berbagai pokok keahlian untuk membuat produk perangkat lunak'],
            ['name' => 'Support Fullstack Developer','is_developer' => 1, 'desc' => 'Adalah pengembang perangkat lunak pendukung Fullstack utama'],
            ['name' => 'Main Back End Developer','is_developer' => 1, 'desc' => 'Adalah pengembang perangkat lunak yang berfokus pada bagian Back End'],
            ['name' => 'Support Back End Developer','is_developer' => 1, 'desc' => 'Adalah pengembang perangkat lunak pendukung pengembang Back End utama'],
            ['name' => 'Main Front End Developer','is_developer' => 1, 'desc' => 'Adalah pengembang perangkat lunak yang berfokus pada bagian Front End'],
            ['name' => 'Support Front End Developer','is_developer' => 1, 'desc' => 'Adalah pengembang perangkat lunak pendukung pengembang Front End utama'],
            ['name' => 'Customer','is_developer' => 0, 'desc' => 'Adalah pelanggan yang meminta dibuatkan perangkat lunak'],
        ];

        foreach ($roles as $role) {
            Role::create($role);
        }
    }
}
