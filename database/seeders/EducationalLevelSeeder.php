<?php

namespace Database\Seeders;

use App\Models\EducationalLevel;
use Illuminate\Database\Seeder;

class EducationalLevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $levels = [
            ['name' => 'SD Sederajat','desc' => 'SD / MI'],
            ['name' => 'SMP Sederajat','desc' => 'SMP / MTs'],
            ['name' => 'SMA Sederajat','desc' => 'SMA / SMK / SMU / MA / MAK'],
            ['name' => 'D1','desc' => 'Deploma 1'],
            ['name' => 'D2','desc' => 'Deploma 2'],
            ['name' => 'D3','desc' => 'Deploma 3'],
            ['name' => 'D4','desc' => 'Deploma 4'],
            ['name' => 'S1','desc' => 'Strata 1'],
            ['name' => 'S2','desc' => 'Strata 2'],
            ['name' => 'S3','desc' => 'Strata 3'],
        ];
        foreach ($levels as $dt) {
            EducationalLevel::create($dt);
        }
    }
}
