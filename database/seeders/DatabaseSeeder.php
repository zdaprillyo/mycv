<?php

namespace Database\Seeders;

use App\Models\Contact;
use App\Models\Database;
use App\Models\EducationalLevel;
use App\Models\EducationRecord;
use App\Models\Framework;
use App\Models\Language;
use App\Models\Library;
use App\Models\Profile;
use App\Models\Project;
use App\Models\Religion;
use App\Models\Role;
use App\Models\SocialMedia;
use App\Models\User;
use App\Models\WorkExperience;
use App\Models\WorkType;
use Illuminate\Database\Seeder;
use Hash;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        User::truncate();
        Contact::truncate();
        SocialMedia::truncate();
        Religion::truncate();
        WorkType::truncate();
        Framework::truncate();
        Language::truncate();
        Database::truncate();
        Library::truncate();
        Role::truncate();
        EducationalLevel::truncate();
        EducationRecord::truncate();
        WorkExperience::truncate();
        Project::truncate();
        Profile::truncate();
        Schema::enableForeignKeyConstraints();

        User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'email_verified_at' => now(),
            'password' => Hash::make('12345')
        ]);
        $databases = ['MySQL','PostgreSQL'];
        foreach ($databases as $db) {
            Database::create([
                'name' => $db
            ]);
        }
        $this->call([
            SocialMediaSeeder::class,
            ContactSeeder::class,
            ReligionSeeder::class,
            WorkTypeSeeder::class,
            LanguageSeeder::class,
            FrameworkSeeder::class,
            LibrarySeeder::class,
            RoleSeeder::class,
            EducationalLevelSeeder::class,
            EducationRecordSeeder::class,
            WorkExperienceSeeder::class,
            ProjectSeeder::class,
            ProfileSeeder::class
        ]);
    }
}
