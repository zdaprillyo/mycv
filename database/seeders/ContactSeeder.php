<?php

namespace Database\Seeders;

use App\Models\Contact;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contacts = [
            ['name' => 'Zoga Dwi Aprillyo',
             'address' => 'Jalan Rungkut Mejoyo Selatan X, Blok H9, Kali Rungkut, Rungkut, Kota Surabaya, Jawa Timur',
             'is_developer' => 1,
             'legal_entity' => 'Perorangan',
             'user_id' => 1,
             'photo' => null
            ],
            ['name' => 'Fathul Husnan',
             'address' => 'Jalan Rungkut Mejoyo Selatan X, Blok H9, Kali Rungkut, Rungkut, Kota Surabaya, Jawa Timur',
             'is_developer' => 1,
             'legal_entity' => 'Perorangan',
             'user_id' => null,
             'photo' => null
            ],
            ['name' => 'CV Createch',
             'address' => 'Kota Surabaya, Jawa Timur',
             'is_developer' => 0,
             'legal_entity' => 'CV',
             'user_id' => null,
             'photo' => null
            ],
            ['name' => 'CV Pilarmedia',
             'address' => 'Kota Surabaya, Jawa Timur',
             'is_developer' => 0,
             'legal_entity' => 'CV',
             'user_id' => null,
             'photo' => null
            ],
        ];
        $sosmeds = [
            ['contact_id' => 1,
             'social_media_id' => 1,
             'account' => '123',
             'link' => '',
             'created_at' => now(),
             'updated_at' => now()
            ],
            ['contact_id' => 2,
             'social_media_id' => 1,
             'account' => '123',
             'link' => '',
             'created_at' => now(),
             'updated_at' => now()
            ],
            ['contact_id' => 3,
             'social_media_id' => 1,
             'account' => '123',
             'link' => '',
             'created_at' => now(),
             'updated_at' => now()
            ],
        ];
        foreach ($contacts as $contact) {
            Contact::create($contact);
        }
        foreach ($sosmeds as $sosmed) {
            DB::table('contact_social_media')->insert($sosmed);
        }
    }
}
