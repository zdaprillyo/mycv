<?php

namespace Database\Seeders;

use App\Models\Profile;
use Illuminate\Database\Seeder;

class ProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $p = Profile::create([
            'name' => 'Zoga Dwi Aprillyo',
            'religion' => 'Islam',
            'sex' => 'Laki laki',
            'marital_status' => 'Belum Kawin',
            'birth_place' => 'Jombang',
            'birth_date' => '1999-04-23',
            'contact_id' => 1
        ]);
        $p->databases()->sync([1,2]);
        $p->frameworks()->sync([1,4,8,9]);
        $p->libraries()->sync([1,2,3,4]);
        $p->languages()->sync([1,2,3,4,5,6]);
    }
}
