<?php

namespace Database\Seeders;

use App\Models\WorkExperience;
use Illuminate\Database\Seeder;

class WorkExperienceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $experiences = [
            ['contact_id' => '3',
             'work_type_id' => '4',
             'role_id' => 1,
             'start_at' => now(),
             'end_at' => null,
             'desc' => null
            ],
            ['contact_id' => '4',
             'work_type_id' => '1',
             'role_id' => 1,
             'start_at' => now(),
             'end_at' => null,
             'desc' => null
            ],
        ];

        foreach ($experiences as $exp) {
           WorkExperience::create($exp);
        }
    }
}
