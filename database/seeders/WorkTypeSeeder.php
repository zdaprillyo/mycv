<?php

namespace Database\Seeders;

use App\Models\WorkType;
use Illuminate\Database\Seeder;

class WorkTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $work_types = [
            ['name' => 'Purnawaktu','desc' => 'Purnawaktu'],
            ['name' => 'Paruh Waktu','desc' => 'Paruh Waktu'],
            ['name' => 'Wiraswasta','desc' => 'Wiraswasta'],
            ['name' => 'Pekerja Lepas','desc' => 'Pekerja Lepas'],
            ['name' => 'Kontrak','desc' => 'Kontrak'],
            ['name' => 'Magang','desc' => 'Magang'],
            ['name' => 'Seasonal','desc' => 'Seasonal']
        ];
        foreach ($work_types as $type) {
            WorkType::create($type);
        }
    }
}
