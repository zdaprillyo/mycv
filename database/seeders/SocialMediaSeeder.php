<?php

namespace Database\Seeders;

use App\Models\SocialMedia;
use Illuminate\Database\Seeder;

class SocialMediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $social_medias = [
            ['name' => 'Facebook','icon' => 'mdi mdi-facebook-box'],
            ['name' => 'Instagram','icon' => 'mdi mdi-instagram'],
            ['name' => 'Linkedin','icon' => 'mdi mdi-linkedin-box'],
            ['name' => 'Twitter','icon' => 'mdi mdi-twitter-box'],
            ['name' => 'Whatsapp','icon' => 'mdi mdi-whatsapp'],
            ['name' => 'Telegram','icon' => 'mdi mdi-telegram'],
        ];

        foreach ($social_medias as $sosMed) {
            SocialMedia::create($sosMed);
        }
    }
}
