<?php

namespace Database\Seeders;

use App\Models\Project;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'name','link','database_id','is_commercial','desc','start_at','end_at'

        $projects = [
            ['name' => 'Manajemen Rantai Pasok dan Perencanaan Keuangan',
             'link' => null,
             'database_id' => 1,
             'is_commercial' => 0,
             'desc' => 'Manajemen Rantai Pasok dan Perencanaan Keuangan adalah suatu sistem
                yang dapat menangani permintaan pengambilan barang dan order barang dari
                pelanggan UD Dua Saudara. Sistem ini dapat diakses oleh 3 jenis akun, yaitu
                Administrator, Pengepul, dan Konsumen. Jenis akun Administrator akan digunakan
                oleh perusahaan. Jenis akun Pengepul akan digunakan oleh supplyer perusahaan,
                dan jenis akun Konsumen akan digunakan oleh konsumen (pembeli) perusahaan.
                Sistem ini mampu menangani proses pengadaan barang, proses order dari pembeli,
                proses pembayaran, hingga barang tersebut dikirimkan ke konsumen.',
             'start_at' => '2019-01-01',
             'end_at' => now()
            ],
            ['name' => 'Sistem Informasi Pencatatan Administratif',
             'link' => null,
             'database_id' => 1,
             'is_commercial' => 0,
             'desc' => 'Sistem Informasi Pencatatan Administratif adalah sebuah sistem yang
                dirancang untuk dapat mencatat transaksi pembelian dan penjualan yang terjadi.
                Selain itu, sistem ini juga mampu untuk membuat laporan keuangan yang terjadi
                berdasarkan tanggal atau rentang tanggal yang ditentukan. Sistem ini memiliki 1
                jenis akun, yaitu Administrator.',
             'start_at' => '2019-01-02',
             'end_at' => now()
            ],
            ['name' => 'CTA to Auction',
            'link' => null,
            'database_id' => 1,
            'is_commercial' => 0,
            'desc' => 'CTA to Auction adalah suatu sistem pasar daring (marketplace)
                purwarupa yang dirancang untuk kegiatan lelang melelang. Dengan sistem
                ini, pengguna dapat memulai lelang baru atau mengikuti lelang yang sudah
                ada.',
            'start_at' => '2019-01-03',
            'end_at' => now()
           ],
           ['name' => 'Instantgram',
            'link' => null,
            'database_id' => 1,
            'is_commercial' => 0,
            'desc' => 'Instangram adalah suatu sistem media sosial yang dibuat berdasarkan
                media sosial nyata Instagram. Sistem ini memiliki kemampuan dasar
                Instagram seperti, posting, komentar, dan like.
                ',
            'start_at' => '2019-01-04',
            'end_at' => now()
           ],
           ['name' => ' Manajemen Property',
            'link' => null,
            'database_id' => 1,
            'is_commercial' => 0,
            'desc' => 'Manajemen Property adalah sistem yang dirancang untuk manajemen
                property yang ingin dijual atau disewakan. Sistem ini akan menunjukkan
                lokasi property yang dimaksud dalam bentuk poligon berwarna (Cth. Warna
                Hijau) di dalam map. Di dalam map juga akan menampilkan titik atau ikon
                yang mewakili lokasi penting seperti Sekolah, Mall, Pasar, Tempat Wisata,
                dan Restoran. Sistem ini memiliki 1 jenis akun, yaitu Administrator.',
            'start_at' => '2019-01-04',
            'end_at' => now()
           ],
           ['name' => ' Sistem Payroll Berdasarkan Absensi Karyawan',
            'link' => null,
            'database_id' => 1,
            'is_commercial' => 1,
            'desc' => 'Sistem Payroll Berdasarkan Absensi Karyawan adalah sistem yang
                dirancang untuk dapat menangani absensi karyawan, kemudian melakukan
                perhitungan gaji berdasarkan absensi tersebut. Proses absensi karyawan
                dilakukan dengan cara memasukkan nomor id karyawan. Berdasarkan
                absensi karyawan, selanjutnya akan dihitung performa tiap karyawan.
                Performa yang dihitung adalah total masuk kerja, total absen, total waktu
                lembur, dan total terlambat masuk. Selanjutnya, dihitunglah gaji tiap
                karyawan berdasarkan performanya.',
            'start_at' => '2019-01-05',
            'end_at' => now()
           ],
        ];
        foreach ($projects as $data) {
            Project::create($data);
        }
    }
}
