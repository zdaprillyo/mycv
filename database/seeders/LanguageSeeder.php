<?php

namespace Database\Seeders;

use App\Models\Language;
use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages = [
            ['name' => 'PHP','icon' => 'mdi mdi-language-php'],
            ['name' => 'Javascript','icon' => 'mdi mdi-language-javascript'],
            ['name' => 'CSS','icon' => 'mdi mdi-language-css3'],
            ['name' => 'HTML','icon' => ' mdi mdi-language-html5'],
            ['name' => 'C#','icon' => 'mdi mdi-language-csharp'],
            ['name' => 'Java','icon' => 'mdi mdi-language-java'],
        ];
        foreach ($languages as $language) {
            Language::create($language);
        }
    }
}
