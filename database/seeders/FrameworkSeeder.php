<?php

namespace Database\Seeders;

use App\Models\Framework;
use Illuminate\Database\Seeder;

class FrameworkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $libraries = [
            ['name' => 'Laravel','language_id' => 1],
            ['name' => 'Codeigniter','language_id' => 1],
            ['name' => 'Lumen','language_id' => 1],
            ['name' => 'Angular','language_id' => 2],
            ['name' => 'React Js','language_id' => 2],
            ['name' => 'Node Js','language_id' => 2],
            ['name' => 'Vue Js','language_id' => 2],
            ['name' => 'Bootstrap','language_id' => 3],
            ['name' => 'Tailwind','language_id' => 3],
        ];
        foreach ($libraries as $dt) {
            Framework::create($dt);
        }
    }
}
