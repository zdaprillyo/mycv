<?php

namespace Database\Seeders;

use App\Models\Library;
use Illuminate\Database\Seeder;

class LibrarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $libraries = [
            ['name' => 'Laravel Excel','desc' => 'Adalah library yang digunakan untuk import dan export file bertipe .xlx / .xlsx'],
            ['name' => 'Laravel PDF','desc' => 'Adalah library yang digunakan untuk membuat dan menyimpan file berbentuk .pdf'],
            ['name' => 'Yajra Datatable','desc' => 'Adalah library yang digunakan untuk olah tabel berbasis laravel'],
            ['name' => 'Sweatalert2','desc' => 'Adalah library yang digunakan untuk menampilkan alert / notifikasi'],
        ];
        foreach ($libraries as $dt) {
            Library::create($dt);
        }
    }
}
