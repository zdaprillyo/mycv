<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFrameworkProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('framework_project', function (Blueprint $table) {
            $table->foreignId('framework_id')->constrained();
            $table->foreignId('project_id')->constrained();
            $table->primary(['framework_id', 'project_id']);
            $table->index(['framework_id', 'project_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('framework_project');
    }
}
