<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_project', function (Blueprint $table) {
            $table->foreignId('contact_id')->constrained();
            $table->foreignId('project_id')->constrained();
            $table->foreignId('role_id')->constrained();
            $table->primary(['contact_id', 'project_id']);
            $table->index(['contact_id', 'project_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_project');
    }
}
