<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfileToolTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_tool', function (Blueprint $table) {
            $table->foreignId('profile_id')->constrained();
            $table->foreignId('tool_id')->constrained();
            $table->primary(['profile_id', 'tool_id']);
            $table->index(['profile_id', 'tool_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_tool');
    }
}
