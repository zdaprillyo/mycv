<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFrameworkProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('framework_profile', function (Blueprint $table) {
            $table->foreignId('framework_id')->constrained();
            $table->foreignId('profile_id')->constrained();
            $table->primary(['framework_id', 'profile_id']);
            $table->index(['framework_id', 'profile_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('framework_profile');
    }
}
