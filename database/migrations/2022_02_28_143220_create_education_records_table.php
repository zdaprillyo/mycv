<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEducationRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education_records', function (Blueprint $table) {
            $table->id();
            $table->string('institution_name');
            $table->date('start_at');
            $table->date('end_at')->nullable();
            $table->text('desc')->nullable();
            $table->foreignId('educational_level_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education_records');
    }
}
