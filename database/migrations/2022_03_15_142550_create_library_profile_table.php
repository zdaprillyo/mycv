<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLibraryProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('library_profile', function (Blueprint $table) {
            $table->foreignId('library_id')->constrained();
            $table->foreignId('profile_id')->constrained();
            $table->primary(['library_id', 'profile_id']);
            $table->index(['library_id', 'profile_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('library_profile');
    }
}
