<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('religion');
            $table->enum('sex',['Laki laki','Perempuan']);
            $table->enum('marital_status',['Belum Kawin','Kawin','Cerai Hidup','Cerai Mati']);
            $table->string('birth_place');
            $table->date('birth_date');
            $table->foreignId('contact_id')->constrained();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
