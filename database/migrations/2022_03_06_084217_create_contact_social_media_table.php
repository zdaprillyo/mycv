<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactSocialMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_social_media', function (Blueprint $table) {
            $table->foreignId('contact_id')->constrained();
            $table->unsignedBigInteger('social_media_id');
            $table->foreign('social_media_id')->references('id')->on('social_medias');
            $table->primary(['contact_id', 'social_media_id']);
            $table->index(['contact_id', 'social_media_id']);
            $table->string('account');
            $table->string('link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_social_media');
    }
}
