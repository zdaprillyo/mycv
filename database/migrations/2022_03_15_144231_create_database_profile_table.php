<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatabaseProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('database_profile', function (Blueprint $table) {
            $table->foreignId('database_id')->constrained();
            $table->foreignId('profile_id')->constrained();
            $table->primary(['database_id', 'profile_id']);
            $table->index(['database_id', 'profile_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('database_profile');
    }
}
