<?php

/**
 *	Convert date to Database Date Format.
 *  @param int $day day number that being converted.
 *  @return string ex: 2020-01-01
**/
function dbDate(string $date): string
{
    return date('Y-m-d',strtotime($date));
}
/**
 *	Convert Indonesian Long Date to Universal Short Date Format.
 *  @param int $day day number that being converted.
 *  @return string ex: 01 Januari 2022 => 2022-02-01
**/
function toUniversalShortDate($date){
    $month = [
        'Januari' => '01',
        'Februari' => '02',
        'Maret' => '03',
        'April' => '04',
        'Mei' => '05',
        'Juni' => '06',
        'Juli' => '07',
        'Agustus' => '08',
        'September' => '09',
        'Oktober' => '10',
        'November' => '11',
        'Desember' => '12'
    ];
    $splited = explode(' ',$date);
    return dbDate($splited[0] .'-'. $month[$splited[1]] .'-'. $splited[2]);
}

/**
 *	Convert number to Indonesian long day.
 *   @param int $day day number that being converted.
 *   @param string $sunday M for Minggu, and A for Ahad.
 *   @return string ex: 1 => Senin
**/
function longDay(int $day, string $sunday = "M"): string
{
    if ($sunday == "M")
        $sunday = "Minggu";
    else if ($sunday == "A")
        $sunday = "Ahad";

    $longDay = [
        0 => $sunday,
        1 => 'Senin',
        2 => 'Selasa',
        3 => 'Rabu',
        4 => 'Kamis',
        5 => 'Jumat',
        6 => 'Sabtu'
    ];

    return $longDay[$day];
}

/**
 *	Convert number to Indonesian long month.
 *   @param int $month month number that being converted.
 *   @return string ex: 1 => Januari
**/
function longMonth(int $month): string
{
    $longMonth = [
        1 => 'Januari',
        2 => 'Februari',
        3 => 'Maret',
        4 => 'April',
        5 => 'Mei',
        6 => 'Juni',
        7 => 'Juli',
        8 => 'Agustus',
        9 => 'September',
        10 => 'Oktober',
        11 => 'November',
        12 => 'Desember'
    ];

    return $longMonth[$month];
}

/**
 *	Convert number to Indonesian short day.
 *   @param int $day day number that being converted.
 *   @param int $length total length string.
 *   @param string $sunday M for Minggu, and A for Ahad.
 *   @return string ex: 1 => Sen
**/
function shortDay(int $day, int $length = 3, string $sunday = "M"): string
{
    return substr(longDay($day,$sunday),0,$length);
}

/**
 *	Convert number to Indonesian sort month.
 *   @param int $month month number that being converted.
 *   @param int $length total length string.
 *   @return string ex: 1 => Jan
**/
function shortMonth(int $month, int $length = 3): string
{
    return substr(longMonth($month), 0, $length);
}

/**
 *	Convert date to Indonesian Long Date Format with day.
 *   @param string $date the date that being converted.
 *   @param string $sunday M for Minggu, and A for Ahad.
 *   @return string ex: 2020-01-22 => Kamis, 22 Januari 2020
**/
function toLongDateDay(string $date, string $sunday = "M"): string
{
    $splited = str_split("0" . date('wdmy', strtotime($date)), 2);
    $dayText = longDay(intval($splited[0]), $sunday);
    $day = $splited[1];
    $month = longMonth(intval($splited[2]));
    $year = date('Y', strtotime($date));

    return "$dayText, $day $month $year";
}

/**
 *	Convert date to Indonesian Long Date Format.
 *   @param string $date the date that being converted.
 *   @return string ex: 2020-01-22 => 22 Januari 2020
**/
function toLongDate(string $date): string
{
    $splited = str_split(date('dmy', strtotime($date)), 2);

    $day = $splited[0];
    $month = longMonth(intval($splited[1]));
    $year = date('Y', strtotime($date));

    return "$day $month $year";
}

/**
 *	Convert date to Indonesian Short Date Format with day, ex: 2020-01-22 => Kam, 22 Jan 2020.
 *   @param string $date the date that being converted.
 *   @param int $dayLength total length string for day.
 *   @param int $monthLength total length string for month.
 *   @param string $sunday M for Minggu, and A for Ahad.
 *   @return string ex: 2020-01-22 => Kam, 22 Jan 2020
**/
function toShortDateDay(string $date, int $dayLength = 3, int $monthLength = 3, string $sunday = "M"): string
{
    $splited = str_split("0" . date('wdmy', strtotime($date)), 2);

    $dayText = shortDay(intval($splited[0]), $dayLength, $sunday);
    $day = $splited[1];
    $month = shortMonth(intval($splited[2]), $monthLength);
    $year = date('Y', strtotime($date));

    return "$dayText, $day $month $year";
}

/**
 *	Convert date to Indonesian Short Date Format.
 *   @param string $date the date that being converted.
 *   @param int $length total length string.
 *   @return string ex: 2020-01-22 => 22 Jan 2020
**/
function toShortDate(string $date, int $length = 3): string
{
    $splited = str_split(date('dmy', strtotime($date)), 2);

    $day = $splited[0];
    $month = shortMonth(intval($splited[1]), $length);
    $year = date('Y', strtotime($date));

    return "$day $month $year";
}

/**
    *	Convert date to Indonesian Long Month and Year Format.
    *   @param string $date the date that being converted.
    *   @return string ex: 2020-01-22 => Januari 2020
**/
function toLongMonthYear(string $date): string
{
    return longMonth((int)date('m',strtotime($date))).' '.date('Y',strtotime($date));
}

/**
 *	Convert time difference to Indonesian.
 *	@param string $firstDate the first date (Y-m-d).
 *	@param string $secondDate the second date (Y-m-d).
 *   @return string ex result: 1 tahun 2 bulan 3 hari
**/
function timeDifference(string $firstDate, string $secondDate): string
{
    $diff = abs(strtotime($secondDate) - strtotime($firstDate));

    $years = floor($diff / (365 * 60 * 60 * 24));
    $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));
    $days = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));

    $text = '';
    if ($years > 0) $text .= " $years tahun";
    if ($months > 0) $text .= " $months bulan";
    $text .= " $days hari";

    return trim($text);
}
/**
 *	Convert number to Indonesian Format Number.
 *	@param int $nominal the number that being converted.
 *	@param int $decimal set the number of decimal points.
 *  @return string ex: 10000 => 10.000
**/
function formatNumber(int $nominal, int $decimal = 0): string
{
    return number_format(abs(intval($nominal)), intval($decimal), ',', '.');
}

/**
 *	Convert number to Indonesian Rupiah.
 *	@param int $nominal the number that being converted.
 *	@param int $decimal set the number of decimal points.
 *   @return string ex: 10000 => Rp 10.000,-
**/
function toRupiah(int $nominal, int $decimal = 0): string
{
    $number = formatNumber($nominal,$decimal) . (($decimal == 0) ? ',-' : '');
    if ($nominal >= 0) {
        return "Rp $number";
    } else {
        return "- Rp $number";
    }
}

/**
 * Helper for function toRupiahInText
 *
 * @param  int $nominal
 * @return string
 */
function denominator(int $nominal): string
{
    $nominal = abs($nominal);
    $oneToEleven = [
        "",
        "satu",
        "dua",
        "tiga",
        "empat",
        "lima",
        "enam",
        "tujuh",
        "delapan",
        "sembilan",
        "sepuluh",
        "sebelas"
    ];

    $text = "";
    if ($nominal < 12) {
        $text = " " . $oneToEleven[$nominal];
    } else if ($nominal < 20) {
        $text = denominator($nominal - 10) . " belas";
    } else if ($nominal < 100) {
        $text = denominator($nominal / 10) . " puluh" . denominator($nominal % 10);
    } else if ($nominal < 200) {
        $text = " seratus" . denominator($nominal - 100);
    } else if ($nominal < 1000) {
        $text = denominator($nominal / 100) . " ratus" . denominator($nominal % 100);
    } else if ($nominal < 2000) {
        $text = " seribu" . denominator($nominal - 1000);
    } else if ($nominal < 1000000) {
        $text = denominator($nominal / 1000) . " ribu" . denominator($nominal % 1000);
    } else if ($nominal < 1000000000) {
        $text = denominator($nominal / 1000000) . " juta" . denominator($nominal % 1000000);
    } else if ($nominal < 1000000000000) {
        $text = denominator($nominal / 1000000000) . " milyar" . denominator($nominal, 1000000000);
    } else if ($nominal < 1000000000000000) {
        $text = denominator($nominal / 1000000000000) . " trilyun" . denominator($nominal, 1000000000000);
    }
    return $text;
}

/**
 *	Convert number to Indonesian Rupiah in text (terbilang).
 *	@param int $nominal the number that being converted.
 *  @return string ex: 10000 => sepuluh ribu rupiah
**/
function toRupiahInText(int $nominal): string
{
    $text = '';
    if ($nominal < 0) $text = "minus " . trim(denominator($nominal));
    else $text = trim(denominator($nominal));
    $text = str_replace("  ", " ", $text);
    $text .= " rupiah";
    if ($text == " rupiah") $text = "nol rupiah";
    return $text;
}

/**
 * Get all province in Indonesia.
 * @return array
 */
function getAllProvince(): array
{
    return [
        "Bali",
        "Banten",
        "Bengkulu",
        "Daerah Istimewa Yogyakarta",
        "Daerah Khusus Ibukota Jakarta",
        "Gorontalo",
        "Jambi",
        "Jawa Barat",
        "Jawa Tengah",
        "Jawa Timur",
        "Kalimantan Barat",
        "Kalimantan Selatan",
        "Kalimantan Tengah",
        "Kalimantan Timur",
        "Kalimantan Utara",
        "Kepulauan Bangka Belitung",
        "Kepulauan Riau",
        "Lampung",
        "Maluku",
        "Maluku Utara",
        "Nanggroe Aceh Darussalam",
        "Nusa Tenggara Barat",
        "Nusa Tenggara Timur",
        "Papua",
        "Papua Barat",
        "Riau",
        "Sulawesi Barat",
        "Sulawesi Selatan",
        "Sulawesi Tengah",
        "Sulawesi Tenggara",
        "Sulawesi Utara",
        "Sumatra Barat",
        "Sumatra Selatan",
        "Sumatra Utara",
    ];
}
function insert_image($folder,$file){
    $fileName = time() . '.' . $file->getClientOriginalExtension();
    $file->move(public_path("/images/$folder/"), $fileName);
    return $fileName;
}

function update_image($folder,$image_url,$file){
    $this->delete_image($folder,$image_url);
    return $this->insert_image($folder,$file);
}

function delete_image($folder,$image_url){
    if (public_path("/images/$folder/") . $image_url !== null) {
        File::delete(public_path("/images/$folder/") . $image_url);
    }
}

