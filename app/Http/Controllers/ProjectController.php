<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Database;
use App\Models\Project;
use App\Models\Library;
use App\Models\Framework;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()) {
            $projects = Project::with('database:id,name')->select('*');
            return datatables($projects)
            ->addIndexColumn()
            ->editColumn('is_new', function ($row) {
                return $row->is_new ? 'Buat baru' : 'Pengembangan';
            })
            ->editColumn('is_commercial', function ($row) {
                return $row->is_commercial ? 'Komersial' : 'Non-komersial';
            })
            ->editColumn('start_at', function ($row) {
                return toLongDate($row->start_at);
            })
            ->editColumn('end_at', function ($row) {
                return $row->end_at ? toLongDate($row->end_at) : 'Sampai Sekarang';
            })
            ->editColumn('action', 'datatables._action_master')
            ->rawColumns(['is_new','is_commercial','start_at','end_at','action'])
            ->toJson();
        }
        $databases = Database::pluck('name','id')->toArray();
        $frameworks = Framework::pluck('name','id')->toArray();
        $libraries = Library::pluck('name','id')->toArray();
        $roles = Role::pluck('name','id')->toArray();
        $contacts = Contact::pluck('name','id')->toArray();
        $select = Project::generateSelect($roles);
        return view('project',compact('databases','frameworks','libraries','roles','contacts','select'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            Project::buat($request->all());
            DB::commit();
            $http_code = 200;
            $response = $this->save_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        $data = [
            'id' => $project->id ?? null,
            'name' => $project->name ?? null,
            'link' => $project->link ?? null,
            'database_id' => $project->database_id ?? null,
            'is_commercial' => $project->is_commercial ?? null,
            'is_new' => $project->is_new ?? null,
            'desc' => $project->desc ?? null,
            'start_at' => toLongDate($project->start_at),
            'end_at' => $project->end_at ? toLongDate($project->end_at) : null,
            'frameworks' => $project->frameworks->toArray() ?? null,
            'libraries' => $project->libraries->toArray() ?? null,
            'participans' => $project->participans,
            'images' => $project->images,
        ];
        $response = $this->show_detail_response($data);
        return response()->json($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        try {
            DB::beginTransaction();
            $project->ubah($request->all());
            DB::commit();
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        try {
            DB::beginTransaction();
            $project->delete();
            DB::commit();
            $http_code = 200;
            $response = $this->delete_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }
}
