<?php

namespace App\Http\Controllers;

use App\Models\SocialMedia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class SocialMediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()) {
            $sosmeds = SocialMedia::all();
            return datatables($sosmeds)
            ->addIndexColumn()
            ->editColumn('name', function ($sosmed) {
                return '<label><i class="'.$sosmed->icon.'"> '.$sosmed->name.' </label>';
            })
            ->editColumn('action', 'datatables._action_master')
            ->rawColumns(['name','action'])
            ->toJson();
        }
        return view('setting.social-media');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            SocialMedia::create([
                'name' => $request->add_name,
                'icon' => $request->add_icon
            ]);
            DB::commit();
            $http_code = 200;
            $response = $this->save_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SocialMedia  $socialMedia
     * @return \Illuminate\Http\Response
     */
    public function show(SocialMedia $socialMedia)
    {
        $data = [
            'id' => $socialMedia->id ?? null,
            'name' => $socialMedia->name ?? null,
            'icon' => $socialMedia->icon ?? null
        ];
        $response = $this->show_detail_response($data);
        return response()->json($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SocialMedia  $socialMedia
     * @return \Illuminate\Http\Response
     */
    public function edit(SocialMedia $socialMedia)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SocialMedia  $socialMedia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SocialMedia $socialMedia)
    {
        try {
            DB::beginTransaction();
            $socialMedia->update([
                'name' => $request->edit_name,
                'icon' => $request->edit_icon
            ]);
            DB::commit();
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SocialMedia  $socialMedia
     * @return \Illuminate\Http\Response
     */
    public function destroy(SocialMedia $socialMedia)
    {
        try {
            DB::beginTransaction();
            $socialMedia->delete();
            DB::commit();
            $http_code = 200;
            $response = $this->delete_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }
}
