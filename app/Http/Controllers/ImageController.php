<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function store(Request $request){
        $path = storage_path('tmp/');
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        $file = $request->file('file');
        $name = $file->getClientOriginalName();
        $file->move($path, $name);
        return response()->json([
            'name' => $file->getClientOriginalName(),
        ]);
    }
}
