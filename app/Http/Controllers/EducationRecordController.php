<?php

namespace App\Http\Controllers;

use App\Models\EducationalLevel;
use App\Models\EducationRecord;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class EducationRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()) {
            $education_records = EducationRecord::all();
            return datatables($education_records)
                ->addIndexColumn()
                ->editColumn('start_at', function ($row) {
                    return toLongMonthYear($row->start_at);
                })
                ->editColumn('end_at', function ($row) {
                    return $row->end_at ? toLongMonthYear($row->end_at) : 'Sampai Sekarang';
                })
                ->editColumn('action', 'datatables._action_master')
                ->rawColumns(['start_at','end_at','action'])
                ->toJson();
        }
        $educational_levels = EducationalLevel::pluck('name','id')->toArray();
        return view('education-record',compact('educational_levels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            EducationRecord::create([
                'institution_name' => $request->add_institution_name,
                'educational_level_id' => $request->add_educational_level_id,
                'start_at' => toUniversalShortDate('01 '.$request->add_start_at),
                'end_at' => $request->add_end_at ? toUniversalShortDate('01 '.$request->add_end_at) : null,
                'desc' => $request->add_desc
            ]);
            DB::commit();
            $http_code = 200;
            $response = $this->save_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EducationRecord  $educationRecord
     * @return \Illuminate\Http\Response
     */
    public function show(EducationRecord $educationRecord)
    {

        $data = [
            'id' => $educationRecord->id ?? null,
            'institution_name' => $educationRecord->institution_name ?? null,
            'educational_level_id' => $educationRecord->educational_level_id ?? null,
            'start_at' => $educationRecord->start_at ? toLongMonthYear($educationRecord->start_at) : null,
            'end_at' => $educationRecord->end_at ? toLongMonthYear($educationRecord->end_at) : null,
            'desc' => $educationRecord->desc ?? null
        ];
        $response = $this->show_detail_response($data);
        return response()->json($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EducationRecord  $educationRecord
     * @return \Illuminate\Http\Response
     */
    public function edit(EducationRecord $educationRecord)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EducationRecord  $educationRecord
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EducationRecord $educationRecord)
    {
        try {
            DB::beginTransaction();
            $educationRecord->update([
                'institution_name' => $request->edit_institution_name,
                'educational_level_id' => $request->edit_educational_level_id,
                'start_at' => toUniversalShortDate('01 '.$request->edit_start_at),
                'end_at' => $request->edit_end_at ? toUniversalShortDate('01 '.$request->edit_end_at) : null,
                'desc' => $request->edit_desc
            ]);
            DB::commit();
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EducationRecord  $educationRecord
     * @return \Illuminate\Http\Response
     */
    public function destroy(EducationRecord $educationRecord)
    {
        try {
            DB::beginTransaction();
            $educationRecord->delete();
            DB::commit();
            $http_code = 200;
            $response = $this->delete_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }
}
