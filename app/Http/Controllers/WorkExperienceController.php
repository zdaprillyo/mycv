<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Role;
use App\Models\WorkExperience;
use App\Models\WorkType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class WorkExperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()) {
            $experiences = WorkExperience::with('work_type:id,name','role:id,name','contact:id,name')->get();
            return datatables($experiences)
                ->addIndexColumn()
                ->editColumn('start_at', function ($exp) {
                    return toLongDate($exp->start_at);
                })
                ->editColumn('end_at', function ($exp) {
                    return $exp->end_at ? toLongDate($exp->end_at) : 'Sampai Sekarang';
                })
                ->editColumn('action', 'datatables._action_master')
                ->rawColumns(['start_at','end_at','action'])
                ->toJson();
        }
        $work_types = WorkType::pluck('name','id')->toArray();
        $roles = Role::pluck('name','id')->toArray();
        $contacts = Contact::where('is_developer',0)->pluck('name','id')->toArray();
        return view('work-experience',compact('work_types','roles','contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            WorkExperience::create([
                'contact_id' => $request->add_contact_id,
                'work_type_id' => $request->add_work_type_id,
                'role_id' => $request->add_role_id,
                'start_at' => toUniversalShortDate($request->add_start_at),
                'end_at' => $request->add_end_at ? toUniversalShortDate($request->add_end_at) : null,
                'desc' => $request->add_desc
            ]);
            DB::commit();
            $http_code = 200;
            $response = $this->save_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WorkExperience  $workExperience
     * @return \Illuminate\Http\Response
     */
    public function show(WorkExperience $workExperience)
    {
        $data = [
            'id' => $workExperience->id ?? null,
            'contact_id' => $workExperience->contact_id ?? null,
            'work_type_id' => $workExperience->work_type_id ?? null,
            'role_id' => $workExperience->role_id ?? null,
            'start_at' => $workExperience->start_at ? toLongDate($workExperience->start_at) : null,
            'end_at' => $workExperience->end_at ? toLongDate($workExperience->end_at) : null,
            'desc' => $workExperience->desc ?? null
        ];
        $response = $this->show_detail_response($data);
        return response()->json($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WorkExperience  $workExperience
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkExperience $workExperience)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WorkExperience  $workExperience
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorkExperience $workExperience)
    {
        try {
            DB::beginTransaction();
            $workExperience->update([
                'contact_id' => $request->edit_contact_id,
                'work_type_id' => $request->edit_work_type_id,
                'role_id' => $request->edit_role_id,
                'start_at' => toUniversalShortDate($request->edit_start_at),
                'end_at' => $request->edit_end_at ? toUniversalShortDate($request->edit_end_at) : null,
                'desc' => $request->edit_desc
            ]);
            DB::commit();
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WorkExperience  $workExperience
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkExperience $workExperience)
    {
        try {
            DB::beginTransaction();
            $workExperience->delete();
            DB::commit();
            $http_code = 200;
            $response = $this->delete_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }
}
