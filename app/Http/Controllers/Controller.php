<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function show_detail_response($data){
        $msg = '';
        $success = false;
        if(isset($data['id'])){
            $msg = 'OK';
            $success = true;
        }else{
            $msg = "Data tidak ditemukan";
            $success = false;
        }
        return [
            'success' => $success,
            'message' => $msg,
            'data' => $data
        ];
    }

    public function save_response(){
        return $this->general_success_msg('c');
    }
    public function update_response(){
        return $this->general_success_msg('u');
    }
    public function delete_response(){
        return $this->general_success_msg('d');
    }
    public function general_success_msg($action){
        $msg = '';
        switch ($action) {
            case 'c': $msg = 'Tambah'; break;
            case 'u': $msg = 'Ubah'; break;
            case 'd': $msg = 'Hapus'; break;
            default: $msg = 'Tambah'; break;
        }
        return [
            'success' => true,
            'message' => "$msg data berhasil"
        ];
    }

    public function error_msg($e){
        return [
            'success' => false,
            'message' => "Terjadi kesalahan pada server. Segera hubungi pihak penyedia layanan anda!",
            'error' => $e->getCode()."-".$e->getMessage(),
        ];
    }
}
