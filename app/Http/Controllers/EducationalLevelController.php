<?php

namespace App\Http\Controllers;

use App\Models\EducationalLevel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class EducationalLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()) {
            $educational_levels = EducationalLevel::all();
            return datatables($educational_levels)
                ->addIndexColumn()
                ->editColumn('action', 'datatables._action_master')
                ->rawColumns(['action'])
                ->toJson();
        }
        return view('setting.educational-level');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            EducationalLevel::create([
                'name' => $request->add_name,
                'desc' => $request->add_desc
            ]);
            DB::commit();
            $http_code = 200;
            $response = $this->save_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EducationalLevel  $educationalLevel
     * @return \Illuminate\Http\Response
     */
    public function show(EducationalLevel $educationalLevel)
    {
        $data = [
            'id' => $educationalLevel->id ?? null,
            'name' => $educationalLevel->name ?? null,
            'desc' => $educationalLevel->desc ?? null
        ];
        $response = $this->show_detail_response($data);
        return response()->json($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EducationalLevel  $educationalLevel
     * @return \Illuminate\Http\Response
     */
    public function edit(EducationalLevel $educationalLevel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EducationalLevel  $educationalLevel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EducationalLevel $educationalLevel)
    {
        try {
            DB::beginTransaction();
            $educationalLevel->update([
                'name' => $request->edit_name,
                'desc' => $request->edit_desc
            ]);
            DB::commit();
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EducationalLevel  $educationalLevel
     * @return \Illuminate\Http\Response
     */
    public function destroy(EducationalLevel $educationalLevel)
    {
        try {
            DB::beginTransaction();
            $educationalLevel->delete();
            DB::commit();
            $http_code = 200;
            $response = $this->delete_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }
}
