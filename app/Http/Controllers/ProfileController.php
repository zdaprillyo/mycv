<?php

namespace App\Http\Controllers;

use App\Models\Database;
use App\Models\Framework;
use App\Models\Language;
use App\Models\Library;
use App\Models\Profile;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sexs = ['Laki laki' => 'Laki laki','Perempuan' => 'Perempuan'];
        $statuses = ['Belum Kawin' => 'Belum Kawin','Kawin' => 'Kawin','Cerai Hidup' => 'Cerai Hidup','Cerai Mati' => 'Cerai Mati'];
        $databases = Database::pluck('name','id')->toArray();
        $frameworks = Framework::pluck('name','id')->toArray();
        $libraries = Library::pluck('name','id')->toArray();
        $languages = Language::pluck('name','id')->toArray();
        $profile = Profile::with('contact:id,name,address','frameworks:id,name','databases:id,name','languages:id,name','libraries:id,name')->whereId(1)->first();
        return view('setting.profile',compact('sexs','statuses','databases','frameworks','libraries','languages','profile'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        try {
            DB::beginTransaction();
            $profile->ubah($request->all());
            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
    }
}
