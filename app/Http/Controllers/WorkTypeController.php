<?php

namespace App\Http\Controllers;

use App\Models\WorkType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class WorkTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()) {
            $work_types = WorkType::all();
            return datatables($work_types)
                ->addIndexColumn()
                ->editColumn('action', 'datatables._action_master')
                ->rawColumns(['action'])
                ->toJson();
        }
        return view('setting.work-type');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            WorkType::create([
                'name' => $request->add_name,
                'desc' => $request->add_desc
            ]);
            DB::commit();
            $http_code = 200;
            $response = $this->save_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WorkType  $workType
     * @return \Illuminate\Http\Response
     */
    public function show(WorkType $workType)
    {
        $data = [
            'id' => $workType->id ?? null,
            'name' => $workType->name ?? null,
            'desc' => $workType->desc ?? null
        ];
        $response = $this->show_detail_response($data);
        return response()->json($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WorkType  $workType
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkType $workType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WorkType  $workType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorkType $workType)
    {
        try {
            DB::beginTransaction();
            $workType->update([
                'name' => $request->edit_name,
                'desc' => $request->edit_desc
            ]);
            DB::commit();
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WorkType  $workType
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkType $workType)
    {
        try {
            DB::beginTransaction();
            $workType->delete();
            DB::commit();
            $http_code = 200;
            $response = $this->delete_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }
}
