<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\SocialMedia;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->ajax()) {
            $contacts = Contact::all();
            return datatables($contacts)
                ->addIndexColumn()
                ->editColumn('is_developer', function ($row) {
                    return $row->is_developer ? 'Developer' : 'Non Developer';
                })
                ->editColumn('action', 'datatables._action_master')
                ->rawColumns(['is_developer','action'])
                ->toJson();
        }
        $legal_entities = ['Perorangan' => 'Perorangan','CV' => 'CV','PT' => 'PT','Yayasan' => 'Yayasan'];
        $sosmeds = SocialMedia::pluck('name','id')->toArray();
        return view('contact',compact('legal_entities','sosmeds'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        try {
            DB::beginTransaction();
            $c = Contact::create([
                'name' => $request->add_name,
                'address' => $request->add_address,
                'is_developer' => $request->add_is_developer ? 1 : 0,
                'legal_entity' => $request->add_legal_entity,
                'user_id' => $request->add_user_id ?? null,
                'photo' => $request->add_photo ?? null,
            ]);
            $c->updateSocialMedias($request);
            DB::commit();
            $http_code = 200;
            $response = $this->save_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        $sosmeds = [];
        foreach ($contact->social_medias as $sosmed) {
            $sosmeds[] = [
                'id' => $sosmed->id,
                'account' => $sosmed->pivot->account,
                'link'  => $sosmed->pivot->link
            ];
        }
        $data = [
            'id' => $contact->id ?? null,
            'name' => $contact->name ?? null,
            'address' => $contact->address ?? null,
            'is_developer' => $contact->is_developer ?? null,
            'legal_entity' => $contact->legal_entity ?? null,
            'sosmed' => $sosmeds
        ];
        $response = $this->show_detail_response($data);
        return response()->json($response,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        try {
            DB::beginTransaction();
            $contact->update([
                'name' => $request->edit_name,
                'address' => $request->edit_address,
                'is_developer' => $request->edit_is_developer ? 1 : 0,
                'legal_entity' => $request->edit_legal_entity,
                'user_id' => $request->edit_user_id ?? null,
                'photo' => $request->edit_photo ?? null,
            ]);
            $update_request = new Request();
            $update_request->sosmeds = $request->edit_sosmeds;
            $update_request->akun = $request->edit_akun;
            $update_request->link = $request->edit_link;
            $contact->updateSocialMedias($update_request);
            DB::commit();
            $http_code = 200;
            $response = $this->update_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        try {
            DB::beginTransaction();
            $contact->social_medias()->detach();
            $contact->delete();
            DB::commit();
            $http_code = 200;
            $response = $this->delete_response();
        } catch (Exception $e) {
            DB::rollback();
            $http_code = $e->getCode()==422 ? 422 : 421;
            $response = $this->error_msg($e);
        }
        return response()->json($response,$http_code);
    }
}
