<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Framework extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','language_id'
    ];
    public function language()
    {
        return $this->belongsTo(Language::class);
    }
    public function projects(){
        return $this->belongsToMany(Project::class)->withTimestamps();
    }
    public function profiles(){
        return $this->belongsToMany(Profile::class)->withTimestamps();
    }
}
