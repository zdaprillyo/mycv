<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EducationRecord extends Model
{
    use HasFactory;
    protected $fillable = [
        'institution_name','start_at','end_at','desc','educational_level_id'
    ];
}
