<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Contact extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','address','is_developer','legal_entity','user_id','photo'
    ];
    public function social_medias()
    {
        return $this->belongsToMany(SocialMedia::class)->withPivot(['account','link'])->withTimestamps();
    }
    public function work_experiences()
    {
        return $this->hasMany(WorkExperience::class);
    }
    public function projects(){
        return $this->belongsToMany(Project::class)->withPivot('role_id')->withTimestamps();
    }
    public function profile(){
        return $this->hasOne(Profile::class);
    }
    public function updateSocialMedias(Request $request){
        $params = [];
        $loops = count($request->sosmeds) ?? 0;
        for ($i=0; $i < $loops; $i++) {
            $params[$request->sosmeds[$i]] = [
                'account' => $request->akun[$i],
                'link' => $request->link[$i],
            ];
        }
        $this->social_medias()->sync($params);
    }
}
