<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','is_developer','desc'
    ];
    public function work_experiences()
    {
        return $this->hasMany(WorkExperience::class);
    }
}
