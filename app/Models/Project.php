<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use File;

class Project extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','link','database_id','is_commercial','is_new','desc','start_at','end_at'
    ];
    public function getParticipansAttribute(){
        $participans = null;
        foreach ($this->contacts as $contact) {
            $role_id = $contact->pivot->role_id;
            $contact = (object)( ['id' => $contact->id, 'name' => $contact->name]);
            $role = (object)( ['id' => $role_id, 'name' => Role::find($role_id)->name ?? '']);
            $participans[] = collect(['contact' => $contact,'role' => $role]);
        }
        return $participans;
    }
    public function database(){
        return $this->belongsTo(Database::class);
    }
    public function frameworks(){
        return $this->belongsToMany(Framework::class)->withTimestamps();
    }
    public function libraries(){
        return $this->belongsToMany(Library::class)->withTimestamps();
    }
    public function contacts(){
        return $this->belongsToMany(Contact::class)->withPivot('role_id')->withTimestamps();
    }
    public function images(){
        return $this->hasMany(Image::class);
    }
    public static function generateSelect($options){
        $params = [
            'id' => 'select',
            'name' => 'roles[]',
            'options' => $options,
            'label' => null,
            'helper' => null,
            'value' => null,
            'placeholder' => null,
            'selectedNone' => null,
            'single' => null,
            'empty' => null,
            'required' => 'required',
            'disabled' => null,
            'readonly' => null,
            'multiple' => null,
        ];
        return ['html' => view('components.select',$params)->render()];
    }
    private static function fetchDetail($args = []){
        $participans = [];
        for ($i=0; $i < count($args->participans); $i++) {
            $participans[$args->participans[$i]] = ['role_id' => $args->roles[$i]];
        }
        return (object)[
            'participans' => $participans,
            'roles' => $args->roles ?? null,
            'libraries' => $args->add_libraries ?? $args->edit_libraries ?? null,
            'frameworks' => $args->add_frameworks ?? $args->edit_frameworks ?? null
        ];
    }
    private function updateParticipans($params = []){
        $this->contacts()->sync($params);
    }
    private function updateFrameworks($params = []){
        $this->frameworks()->sync($params);
    }
    private function updateLibraries($params = []){
        $this->libraries()->sync($params);
    }
    public function updateDetail($params = []){
        $this->updateParticipans($params->participans);
        $this->updateFrameworks($params->frameworks);
        $this->updateLibraries($params->libraries);
    }
    private static function fetch($args = []){
        $params = [];
        $end_at = null;
        $is_new = 0;
        $is_commercial = 0;
        if(isset($args->add_is_new) || isset($args->edit_is_new)){
            $is_new = 1;
        }
        if(isset($args->add_is_commercial) || isset($args->edit_is_commercial)){
            $is_commercial = 1;
        }
        if(isset($args->add_end_at) || isset($args->edit_end_at)){
            $end_at = toUniversalShortDate($args->add_end_at ?? $args->edit_end_at);
        }
        $params['project'] = [
            'name' => $args->add_name ?? $args->edit_name,
            'link' => null,
            'database_id' => $args->add_database_id ?? $args->edit_database_id,
            'is_commercial' => $is_commercial,
            'is_new' => $is_new,
            'desc' => $args->add_desc ?? $args->edit_desc,
            'start_at' => toUniversalShortDate($args->add_start_at ?? $args->edit_start_at),
            'end_at' => $end_at,
        ];
        $params['detail'] = self::fetchDetail($args);
        $params['images'] = $args->dz_image ?? [];
        return (object)$params;
    }
    public static function buat($params = []){
        $request = self::fetch((object)$params);
        $project = self::create($request->project);
        $project->insertImage($request->images);
        $project->updateDetail($request->detail);
        return $project;
    }
    public function ubah($params = []){
        $request = self::fetch((object)$params);
        $this->update($request->project);
        $this->updateImage($request->images);
        $this->updateDetail($request->detail);
        return $this;
    }
    private function moveImage($image){
        $new_name = time().'_'.str_replace(' ','_',$image);
        if(File::move(storage_path('tmp/') . $image,public_path("images/project/") . $new_name)){
            Image::create([
                'name' => $new_name,
                'size' => File::size(public_path("/images/project/") . $new_name),
                'url' => public_path("images/project/") . $new_name,
                'project_id' => $this->id,
            ]);
        }
        return $new_name;
    }
    private function insertImage($images = null){
        if($images){
            foreach ($images as $image) {
                if(File::exists(storage_path('tmp/') . $image)){
                    $this->moveImage($image);
                }
            }
        }
    }
    private function updateImage($images = null){
        $saved_imgs = [];
        foreach ($images as $image) {
            if(File::exists(storage_path('tmp/') . $image)){
                $saved_imgs[] = $this->moveImage($image);
            }else{
                $saved_imgs[] = $image;
            }
        }
        $deleted_imgs = Image::whereProjectId($this->id)->whereNotIn('name',$saved_imgs)->get();
        foreach ($deleted_imgs as $img) {
            if(File::delete(public_path("/images/project/") . $img->name)){
                $img->delete();
            }
        }
    }
}
