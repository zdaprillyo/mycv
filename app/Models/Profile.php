<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','religion','sex','marital_status','birth_place','birth_date','contact_id'
    ];
    public function contact(){
        return $this->belongsTo(Contact::class);
    }
    public function frameworks(){
        return $this->belongsToMany(Framework::class)->withTimestamps();
    }
    public function libraries(){
        return $this->belongsToMany(Library::class)->withTimestamps();
    }
    public function languages(){
        return $this->belongsToMany(Language::class)->withTimestamps();
    }
    public function databases(){
        return $this->belongsToMany(Database::class)->withTimestamps();
    }

    private function fetch($args){
        return (object)[
            'detail' => [
                'name' => $args->name,
                'religion' => $args->religion,
                'sex' => $args->sex,
                'marital_status' => $args->marital_status,
                'birth_place' => $args->birth_place,
                'birth_date' => toUniversalShortDate($args->birth_date)
            ],
            'database' => $args->database ?? [],
            'framework' => $args->framework ?? [],
            'library' => $args->library ?? [],
        ];
    }

    public function ubah($params = []){
        $request = self::fetch((object)$params);
        $this->update($request->detail);
        $this->databases()->sync($request->database);
        $this->frameworks()->sync($request->framework);
        $this->libraries()->sync($request->library);
        return $this;
    }
}
