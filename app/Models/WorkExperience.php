<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WorkExperience extends Model
{
    use HasFactory;
    protected $fillable = [
        'contact_id','work_type_id','role_id','start_at','end_at','desc'
    ];
    public function work_type()
    {
        return $this->belongsTo(WorkType::class);
    }
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }
}
