<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Textarea extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $id,
        private string $name,
        private ?string $label = NULL,
        private ?string $placeholder = NULL,
        private ?string $helper = NULL,
        private ?string $value = NULL,
        private ?string $rows = NULL,
        private ?string $required = NULL,
        private ?string $disabled = NULL,
        private ?string $single = NULL,
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $params = [
            'id' => $this->id,
            'label' => $this->label,
            'placeholder' => $this->placeholder,
            'helper' => $this->helper,
            'name' => $this->name,
            'value' => $this->value,
            'rows' => $this->rows,
            'required' => $this->required,
            'disabled' => $this->disabled,
            'single' => $this->single,
        ];
        return view('components.textarea',$params);
    }
}
