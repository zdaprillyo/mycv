<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Form extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $action,
        private string $method,
        private string $submitIcon = 'fa-solid fa-check',
        private string $submitType = 'success',
        private ?string $sendFile = NULL,
        private ?string $submit = NULL,
        private ?string $submitSmall = NULL,
    )
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $params = [
            'action' => $this->action,
            'method' => $this->method,
            'sendFile' => $this->sendFile,
            'submit' => $this->submit,
            'submitIcon' => $this->submitIcon,
            'submitType' => $this->submitType,
            'submitSmall' => $this->submitSmall,
        ];
        return view('components.form',$params);
    }
}
