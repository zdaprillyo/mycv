<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Input extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $id,
        private string $name,
        private string $type = "text",
        private ?string $numberMin = NULL,
        private ?string $numberMax = NULL,
        private ?string $numberStep = NULL,
        private ?string $placeholder = NULL,
        private ?string $minLength = NULL,
        private ?string $maxLength = NULL,
        private ?string $length = NULL,
        private ?string $pattern = NULL,
        private ?string $value = NULL,
        private ?string $prefix = NULL,
        private ?string $suffix = NULL,
        private ?string $label = NULL,
        private ?string $helper = NULL,
        private ?string $hidden = NULL,
        private ?string $single = NULL,
        private ?string $required = NULL,
        private ?string $disabled = NULL,
        private ?string $readonly = NULL,
        private ?string $messages = NULL,
    ) {
        if ($this->length) {
            $this->minLength = $this->length;
            $this->maxLength = $this->length;
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $params = [
            'id' => $this->id,
            'type' => $this->type,
            'name' => $this->name,
            'numberMin' => $this->numberMin,
            'numberMax' => $this->numberMax,
            'numberStep' => $this->numberStep,
            'placeholder' => $this->placeholder,
            'minLength' => $this->minLength,
            'maxLength' => $this->maxLength,
            'pattern' => $this->pattern,
            'value' => $this->value,
            'prefix' => $this->prefix,
            'suffix' => $this->suffix,
            'label' => $this->label,
            'helper' => $this->helper,
            'hidden' => $this->hidden,
            'single' => $this->single,
            'required' => $this->required,
            'disabled' => $this->disabled,
            'readonly' => $this->readonly,
            'messages' => $this->messages,
        ];
        return view('components.input',$params);
    }
}
