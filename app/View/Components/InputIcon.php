<?php

namespace App\View\Components;

use Illuminate\View\Component;

class InputIcon extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $id,
        private string $name,
        private ? string $label = NUll,
        private ? string $placeholder = NUll,
    )
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $params = [
            'id' => $this->id,
            'name' => $this->name,
            'label' => $this->label,
            'placeholder' => $this->placeholder
        ];
        return view('components.input-icon',$params);
    }
}
