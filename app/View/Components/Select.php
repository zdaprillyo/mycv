<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Select extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        private string $id,
        private string $name,
        private array $options = [],
        private string $selectedNone = "-- Pilih --",
        private ?string $label = NUll,
        private ?string $helper = NUll,
        private ?string $value = NUll,
        private ?string $placeholder = NUll,
        private ?string $single = NULL,
        private ?string $empty = NULL,
        private ?string $required = NULL,
        private ?string $disabled = NULL,
        private ?string $readonly = NULL,
        private ?string $multiple = NULL,
    ) {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $params = [
            'id' => $this->id,
            'name' => $this->name,
            'options' => $this->options,
            'label' => $this->label,
            'helper' => $this->helper,
            'value' => $this->value,
            'placeholder' => $this->placeholder,
            'selectedNone' => $this->selectedNone,
            'single' => $this->single,
            'empty' => $this->empty,
            'required' => $this->required,
            'disabled' => $this->disabled,
            'readonly' => $this->readonly,
            'multiple' => $this->multiple,
        ];
        return view('components.select',$params);
    }
}
