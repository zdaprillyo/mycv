<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Modal extends Component
{
    private $id;
    private $header;
    private $size;
    private $footer;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id,$header,$size = 'md',$footer = null)
    {
        $this->id = $id;
        $this->header = $header;
        $this->size = $size;
        $this->footer = $footer;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $data['id'] = $this->id;
        $data['header'] = $this->header;
        $data['size'] = $this->size;
        $data['footer'] = $this->footer;
        return view('components.modal',$data);
    }
}
