<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Table extends Component
{

    private $id;
    private $head;
    private $body;
    private $foot;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id,$head = null,$body = null, $foot = null)
    {
        $this->id = $id;
        $this->head = $head;
        $this->body = $body;
        $this->foot = $foot;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $data['id'] = $this->id;
        $data['head'] = $this->head;
        $data['body'] = $this->body;
        $data['foot'] = $this->foot;
        return view('components.table',$data);
    }
}
