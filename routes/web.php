<?php

use App\Http\Controllers\ContactController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DatabaseController;
use App\Http\Controllers\EducationalLevelController;
use App\Http\Controllers\EducationRecordController;
use App\Http\Controllers\FrameworkController;
use App\Http\Controllers\ImageController;
use App\Http\Controllers\LanguageController;
use App\Http\Controllers\LibraryController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ReligionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\SocialMediaController;
use App\Http\Controllers\ToolController;
use App\Http\Controllers\WorkExperienceController;
use App\Http\Controllers\WorkTypeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard.index');
    Route::group(['prefix' => 'setting','as' => 'setting.'], function () {
        Route::resource('database',DatabaseController::class);
        Route::resource('tool',ToolController::class);
        Route::resource('language',LanguageController::class);
        Route::resource('framework',FrameworkController::class);
        Route::resource('religion',ReligionController::class);
        Route::resource('social-media',SocialMediaController::class);
        Route::resource('work-type',WorkTypeController::class);
        Route::resource('service',ServiceController::class);
        Route::resource('library',LibraryController::class);
        Route::resource('role',RoleController::class);
        Route::resource('educational-level',EducationalLevelController::class);
    });
    Route::resource('profile',ProfileController::class);
    Route::resource('education-record',EducationRecordController::class);
    Route::resource('work-experience',WorkExperienceController::class);
    Route::resource('contact',ContactController::class);
    Route::resource('project',ProjectController::class);
});
Route::post('image/store',[ImageController::class,'store'])->name('image.store');
Route::get('/', function () {
    return view('client.home');
})->name('client.home');
Route::get('/work', function () {
    return view('client.work-show');
})->name('work.show');

