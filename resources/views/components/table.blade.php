<div class="table-responsive">
    <table id="{{ $id }}" class="table table-borderless table-striped table-dark table-hover table-sm w-100">
        <thead>
            {!! $head !!}
        </thead>
        @isset($body)
            <tbody>
                {!! $body !!}
            </tbody>
        @endisset
        @isset($foot)
            <tfoot>
                {!! $foot !!}
            </tfoot>
        @endisset
    </table>
</div>
