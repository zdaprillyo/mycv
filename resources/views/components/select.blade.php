<div class="form-group @if(!isset($single)) mb-3 @endif" id="container_{{ $id }}">

    @isset($label)
        <label for="{{ $id }}">{{ $label }} @isset($required) <span class="text-danger">*</span> @endisset @isset($helper) <i class="fa-solid fa-circle-question" data-toggle="tooltip" data-placement="top" title="{{ $helper }}"></i> @endisset</label>
    @endisset

    <select
        class="form-control mb-2 @isset($multiple) select-multiple @endisset"
        id="{{ $id }}" name="{{ $name }}"
        data-placeholder="{{ $placeholder }}"
        @isset($required) required @endisset
        @isset($disabled) disabled @endisset
        @isset($readonly) readonly @endisset
        @isset($multiple) multiple="{{ $multiple }}"  style="width:100%" @endisset
        data-parsley-errors-container="#container_{{ $id }}"
    >

    @isset($multiple)
        @php
            $name = str_replace('[]', '', $name);
        @endphp
        @foreach ($options as $key => $option)
            @isset($value)
                <option value="{{ $key }}" {{ (in_array($key, ($value))) ? "selected" : '' }}>{{ $option }}</option>
            @else
                <option value="{{ $key }}" {{ (in_array($key, (old($name) ?? []))) ? "selected" : '' }}>{{ $option }}</option>
            @endisset
        @endforeach
    @else
        @isset($empty)
        @else
            <option selected value="">{{ $selectedNone }}</option>
        @endisset
        @foreach ($options as $key => $option)
            @isset($value)
                <option value="{{ $key }}" {{ ($value == $key) ? "selected" : '' }}>{{ $option }}</option>
            @else
                <option value="{{ $key }}" {{ (old($name) == $key) ? "selected" : '' }}>{{ $option }}</option>
            @endisset
        @endforeach
    @endisset


    </select>
</div>
