<div class="form-group">
    <div class="input-group">
        <input type="text" class="form-control"  id="{{ $id }}" name="{{ $name }}" placeholder="{{ $placeholder }}" aria-label="{{ $label }}" aria-describedby="basic-addon2">
        <div class="input-group-append">
            <button  class="btn btn-sm btn-inverse-primary" type="button">
                <i id="{{ $id }}_privew" class=""></i>
            </button>
        </div>
    </div>
</div>
