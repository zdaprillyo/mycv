<div class="form-check form-check-primary">
    <label class="form-check-label">
    <input type="checkbox" class="form-check-input" id="{{ $id }}" name="{{ $name }}"> {{ $label }}</label>
</div>
