<div class="form-group @if(!isset($single)) mb-3 @endif" id="container_{{ $id }}" @isset($hidden) style="display: none;" @endisset>

    @isset($label)
        <label for="{{ $id }}">{{ $label }} @isset($required) <span class="text-danger">*</span> @endisset @isset($helper) <i class="fa-solid fa-circle-question" data-toggle="tooltip" data-placement="top" title="{{ $helper }}"></i> @endisset</label>
    @endisset

    @if (isset($prefix) || isset($suffix))
        <div class="input-group">
    @endif

    @if (isset($prefix))
        <div class="input-group-prepend">
            <span class="input-group-text">{!! $prefix !!}</span>
        </div>
    @endif

    <input
        @if ($type == 'digits' || $type == 'alphanum' || $type == 'date')
            type="text"
            data-parsley-type="{{ $type }}"
        @elseif ($type == "number")
            type="{{ $type }}"
            data-parsley-type="{{ $type }}"
        @elseif ($type == "url")
            type="{{ $type }}"
            pattern="https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)"
            placeholder="Sertakan http:// atau https:// di awal link"
        @else
            type="{{ $type }}"
        @endif
        class="form-control mb-2 @if ($type == 'date') datepicker @endif"
        name="{{ $name }}" id="{{ $id }}"
        @if ($type == "number")
            @isset($numberMin) min="{{ $numberMin }}" @endisset
            @isset($numberMax) max="{{ $numberMax }}" @endisset
            @isset($numberStep) step="{{ $numberStep }}" @endisset
        @endif
        @isset($pattern) pattern="{{ $pattern }}" @endisset
        @isset($minLength) data-parsley-minlength="{{ $minLength }}" @endisset
        @isset($maxLength) data-parsley-maxlength="{{ $maxLength }}" @endisset
        placeholder="{{ $placeholder }}"
        @if($type != 'password') value="{{ old($name) ?? $value }}" @endif
        @isset($required) required @endisset
        @isset($disabled) disabled @endisset
        @isset($readonly) readonly @endisset
        @isset($messages)
            {{ $messages }}
        @endisset
        data-parsley-trigger="blur"
        @if (explode("_", $name)[count(explode("_", $name)) - 1] == 'confirmation')
            data-parsley-equalto="#{{ str_replace("_confirmation", "", $name) }}"
        @endif
    >

    @if (isset($suffix))
        <div class="input-group-append">
            <span class="input-group-text">{!! $suffix !!}</span>
        </div>
    @endif

    @if (isset($prefix) || isset($suffix))
        </div>
    @endif

</div>
