<div class="modal fade" id="{{ $id }}"  style="width:100%">
    <div class="modal-dialog modal-{{ $size }}">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title h5">
                    <span id="prefix_header_{{ $id }}"></span> {{ $header }} <span id="suffix_header_{{ $id }}"></span>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! $slot !!}
            </div>
            @isset($footer)
                <div class="modal-footer">
                    {!! $footer !!}
                </div>
            @endisset
        </div>
    </div>
</div>
