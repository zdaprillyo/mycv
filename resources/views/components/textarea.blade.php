<div class="form-group @if(!isset($single)) mb-3 @endif" id="container_{{ $id }}">
    @isset($label)
        <label for="{{ $id }}">{{ $label }} @isset($required) <span class="text-danger">*</span> @endisset @isset($helper) <i class="fa-solid fa-circle-question" data-toggle="tooltip" data-placement="top" title="{{ $helper }}"></i> @endisset</label>
    @endisset

    <textarea
        class="summernote form-control"
        name="{{ $name }}" id="{{ $id }}"
        @isset($rows) rows="{{ $rows }}" @endisset
        @isset($disabled) disabled @endisset
        @isset($required) required @endisset
        placeholder="{{ $placeholder }}"
        data-parsley-errors-container="#container_{{ $id }}"
    >{!! $value !!}</textarea>
</div>
