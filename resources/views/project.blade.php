@extends('layouts.admin.app')

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/css/datatables.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/css/my.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/css/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/css/dropzone.min.css') }}">
@endpush

@section('breadcrumb')
    <div class="page-header">
        <h3 class="page-title"> Project </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Project</li>
        </ol>
        </nav>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <p class="card-description">
                <button type="button" class="btn btn-inverse-primary" data-toggle="modal" data-target="#modal_add" onClick="reset()"><i class="mdi mdi-plus"></i>Tambah</button>
            </p>
            <div class="template-demo">
                <x-table id="datatable">
                    @slot('head')
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Peruntukan</th>
                            <th>Sifat</th>
                            <th>Mulai</th>
                            <th>Selesai</th>
                            <th>Aksi</th>
                        </tr>
                    @endslot
                </x-table>
            </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@section('modal')
    <x-modal id="modal_add" header="Form Tambah" size="lg">
        <x-form action="" method="post" submit="Simpan" id="form_add">
            <div class="row">
                <div class="col dropzone m-3" id="add-dropzone">
                </div>
                <div class="col">
                    <x-input id="add_name" name="add_name" label="Nama" placeholder="Manajemen Rantai Pasok"/>
                    <div class="row">
                        <div class="col">
                            <x-checkbox id="add_is_commercial" name="add_is_commercial" label="Komersial"/>
                        </div>
                        <div class="col">
                            <x-checkbox id="add_is_new" name="add_is_new" label="Sistem Baru"/>
                        </div>
                    </div>
                    <x-select id="add_database_id" name="add_database_id" label="Basis Data" :options="$databases"/>
                    <x-input id="add_start_at" name="add_start_at" type="date" label="Mulai" placeholder="Tanggal Masuk"/>
                    <x-input id="add_end_at" name="add_end_at" type="date" label="Selesai" placeholder="Tanggal Selesai"/>
                    <x-textarea id="add_desc" name="add_desc" label="Deskripsi" rows="9" placeholder="Adalah sistem yang dibuat untuk menangani rantai pasok"/>
                </div>
                <div class="col">
                    <x-select id="add_framework_id" name="add_frameworks[]" label="Framework" :options="$frameworks" multiple="multiple"/>
                    <x-select id="add_library_id" name="add_libraries[]" label="Library" :options="$libraries" multiple="multiple"/>
                    <x-select id="add_participans" name="add_participans" label="Partisipan" :options="$contacts" multiple="multiple"/>
                    <x-table id="participan-table">
                        @slot('head')
                            <tr>
                                <th>Nama</th>
                                <th>Peran</th>
                            </tr>
                        @endslot
                        @slot('body')
                        @endslot
                    </x-table>
                </div>
            </div>
        </x-form>
    </x-modal>
    <x-modal id="modal_edit" header="Form Edit" size="lg">
        <x-form action="" method="put" submit="Update" id="form_edit">
            <div class="row">
                <div class="col dropzone m-3" id="edit-dropzone">
                </div>
                <div class="col">
                    <x-input id="edit_name" name="edit_name" label="Nama" placeholder="Manajemen Rantai Pasok"/>
                    <div class="row">
                        <div class="col">
                            <x-checkbox id="edit_is_commercial" name="edit_is_commercial" label="Komersial"/>
                        </div>
                        <div class="col">
                            <x-checkbox id="edit_is_new" name="edit_is_new" label="Sistem Baru"/>
                        </div>
                    </div>
                    <x-select id="edit_database_id" name="edit_database_id" label="Basis Data" :options="$databases"/>
                    <x-input id="edit_start_at" name="edit_start_at" type="date" label="Mulai" placeholder="Tanggal Masuk"/>
                    <x-input id="edit_end_at" name="edit_end_at" type="date" label="Selesai" placeholder="Tanggal Selesai"/>
                    <x-textarea id="edit_desc" name="edit_desc" label="Deskripsi" rows="9" placeholder="Adalah sistem yang dibuat untuk menangani rantai pasok"/>
                </div>
                <div class="col">
                    <div class="col">
                        <x-select id="edit_framework_id" name="edit_frameworks[]" label="Framework" :options="$frameworks" multiple="multiple"/>
                        <x-select id="edit_library_id" name="edit_libraries[]" label="Library" :options="$libraries" multiple="multiple"/>
                        <x-select id="edit_participans" name="edit_participans" label="Partisipan" :options="$contacts" multiple="multiple"/>
                        <x-table id="edit-participan-table">
                            @slot('head')
                                <tr>
                                    <th>Nama</th>
                                    <th>Peran</th>
                                </tr>
                            @endslot
                            @slot('body')
                            @endslot
                        </x-table>
                    </div>
                </div>
            </div>
        </x-form>
    </x-modal>
@endsection

@push('js')
    <script type="text/javascript" src="{{ asset('custom/js/datatables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/id.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/dropzone.min.js') }}"></script>
    <script>
        Dropzone.autoDiscover = false;
        let dz_url = '{{ route('image.store') }}'
        let dz_token = '{{ csrf_token() }}'
        let addDz = generate_dropzone('add-dropzone','form_add',dz_url,dz_token)
        let editDz = generate_dropzone('edit-dropzone','form_edit',dz_url,dz_token)

        var participans = []
        var contacts = @json($contacts);
        let select = @json($select);

        function reset(){
            participans = []
            //add
            $('#add_participans').val([]).trigger('change');
            $('#participan-table tbody').html('')
            //edit
            $('#edit_participans').val([]).trigger('change');
            $('#edit-participan-table tbody').html('')
        }

        function change_value(id,select){
            participans.find( x => x.id == id).role = $('#'+select).val()
        }

        function draw_on_table(params,action = 1){ // action: 1, create|2, update
            let html = ''
            let list_selector = []
            params.forEach(e => {
                let selector = action == 1 ? 'role'+e.id : 'edit_role'+e.id
                html+='<input type=\"hidden\" name="participans[]" value=\"'+e.id+'\" ></td>'
                html+="<tr>"
                html+="<td>"+e.name+"</td>"
                html+="<td id='td_"+selector+"'>"+select.html+"</td>"
                html+="</tr>"
                list_selector.push({ id : e.id,selector : selector, role : e.role ?? null })
            });
            if(action == 1){
                $('#participan-table tbody').html(html)
            }else{
                $('#edit-participan-table tbody').html(html)
            }
            change_attr_select(list_selector)
        }

        function change_attr_select(list_selector){
            if(list_selector.length > 0){
                list_selector.forEach(e => {
                    $('#td_'+e.selector+' select').attr('id','select_'+e.selector)
                    let select_id = '#select_'+e.selector
                    $(select_id).val(e.role)
                    $(select_id).attr('onChange', "change_value('"+e.id+"','select_"+e.selector+"')");
                });
            }
        }

        function show_detail(data) {
            reset()
            $('#edit_name').val(data.name)
            $('#edit_database_id').val(data.database_id)
            $("#edit_is_commercial").prop("checked", data.is_commercial);
            $("#edit_is_new").prop("checked", data.is_new);
            $('#edit_start_at').val(data.start_at)
            $('#edit_end_at').val(data.end_at)
            $('#edit_desc').val(data.desc)
            let temp_framework = []
            let temp_library = []
            let temp_participan = []

            data.frameworks.forEach(e => {
                temp_framework.push(e.id)
            });
            data.libraries.forEach(e => {
                temp_library.push(e.id)
            });
            if(data.participans){
                data.participans.forEach(e => {
                    temp_participan.push(e.contact.id)
                    participans.push({ id : e.contact.id,name:e.contact.name ?? '-', role : e.role.id })
                });
            }
            draw_on_table(participans,2)
            $('#edit_framework_id').val(temp_framework).trigger('change');
            $('#edit_library_id').val(temp_library).trigger('change');
            $('#edit_participans').val(temp_participan).trigger('change');
            set_dropzone_preview(editDz,data.images,"{{ asset('images/project') }}",'form_edit')
        }

        $('#add_participans,#edit_participans').on('select2:select', function (e) {
            $(this).val().forEach(e => {
            if(!participans.find( x => x.id == e)){
                participans.push({ id : e,name:contacts[e] ?? '-', role : '' })
            }
            });
            let action = 1
            if(e.currentTarget.id == 'edit_participans'){
                action = 2
            }
            draw_on_table(participans,action)
        });

        $('#add_participans,#edit_participans').on('select2:unselecting', function (e) {
            let unselected_id = e.params.args.data.id
            participans = participans.filter(function(e, index, arr){
                return e.id != unselected_id;
            });
            let action = 1
            if(e.currentTarget.id == 'edit_participans'){
                action = 2
            }
            draw_on_table(participans,action)
        });

        $(document).ready(function(){
            let index_url = "{{ route('project.index') }}"
            let store_url = "{{ route('project.store') }}"
            let update_url = "{{ route('project.update','-id-') }}"
            let delete_url = "{{ route('project.destroy','-id-') }}"
            let using_ajax_get_request = 1
            $('.select-multiple').select2();

            generate_datetimepicker('.datepicker','DD MMMM YYYY')
            let columns = [
                { data: 'name' },
                { data: 'is_commercial',searchable: false},
                { data: 'is_new',searchable: false},
                { data: 'start_at',searchable: false },
                { data: 'end_at',searchable: false }
            ]
            let table = generate_datatable('Project',index_url,columns,[2, 'desc'])
            generate_update_listener('form_edit',table)
            generate_store_listener('form_add',store_url,table)
            generate_delete_listener(delete_url,table)
            generate_show_listener(update_url,using_ajax_get_request)
        })
    </script>
@endpush
