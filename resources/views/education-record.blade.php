@extends('layouts.admin.app')

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/css/datatables.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('custom/css/my.css') }}">
    <link rel="stylesheet" href="{{ asset('custom/css/bootstrap-datetimepicker.min.css') }}">
@endpush
@section('breadcrumb')
    <div class="page-header">
        <h3 class="page-title"> Education Record </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Education Record</li>
        </ol>
        </nav>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <p class="card-description">
                <button type="button" class="btn btn-inverse-primary" data-toggle="modal" data-target="#modal_add"><i class="mdi mdi-plus"></i>Tambah</button>
            </p>
            <div class="template-demo">
                <x-table id="datatable">
                    @slot('head')
                        <tr>
                            <th>No.</th>
                            <th>Institusi</th>
                            <th>Masuk</th>
                            <th>Selesai</th>
                            <th>Aksi</th>
                        </tr>
                    @endslot
                </x-table>
            </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@section('modal')
    <x-modal id="modal_add" header="Form Tambah">
        <x-form action="" method="post" submit="Simpan" id="form_add">
            <x-input id="add_institution_name" name="add_institution_name" label="Nama Institusi" placeholder="Universitas Surabaya"/>
            <x-select id="add_educational_level_id" name="add_educational_level_id" label="Tingkat" :options="$educational_levels"/>
            <x-input id="add_start_at" name="add_start_at" type="date" label="Masuk" placeholder="Tanggal Masuk"/>
            <x-input id="add_end_at" name="add_end_at" type="date" label="Masuk" placeholder="Tanggal Selesai"/>
            <x-textarea id="add_desc" name="add_desc" label="Deskripsi" placeholder="Lulus dengan gelar S.Kom"/>
        </x-form>
    </x-modal>
    <x-modal id="modal_edit" header="Form Edit">
        <x-form action="" method="put" submit="Update" id="form_edit">
            <x-input id="edit_institution_name" name="edit_institution_name" label="Nama Institusi" placeholder="Universitas Surabaya"/>
            <x-select id="edit_educational_level_id" name="edit_educational_level_id" label="Tingkat" :options="$educational_levels"/>
            <x-input id="edit_start_at" name="edit_start_at" type="date" label="Masuk" placeholder="Tanggal Masuk"/>
            <x-input id="edit_end_at" name="edit_end_at" type="date" label="Masuk" placeholder="Tanggal Selesai"/>
            <x-textarea id="edit_desc" name="edit_desc" label="Deskripsi" placeholder="Lulus dengan gelar S.Kom"/>
        </x-form>
    </x-modal>
@endsection

@push('js')
    <script type="text/javascript" src="{{ asset('custom/js/datatables.min.js') }}"></script>
    <script src="{{ asset('custom/js/moment.js') }}"></script>
    <script src="{{ asset('custom/js/id.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/bootstrap-datetimepicker.min.js') }}"></script>


    <script>
        let index_url = "{{ route('education-record.index') }}"
        let store_url = "{{ route('education-record.store') }}"
        let update_url = "{{ route('education-record.update','-id-') }}"
        let delete_url = "{{ route('education-record.destroy','-id-') }}"
        let using_ajax_get_request = 1

        let columns = [
            { data: 'institution_name' },
            { data: 'start_at',searchable: false },
            { data: 'end_at',searchable: false }
        ]
        function show_detail(data) {
            $('#edit_name').val(data.name)
            $('#edit_institution_name').val(data.institution_name)
            $('#edit_educational_level_id').val(data.educational_level_id)
            $('#edit_start_at').val(data.start_at)
            $('#edit_end_at').val(data.end_at)
            $('#edit_desc').val(data.desc)
        }
        generate_datetimepicker('.datepicker','MMMM YYYY')

        let table = generate_datatable('Education Record',index_url,columns,[2, 'desc'])
        generate_update_listener('form_edit',table)
        generate_store_listener('form_add',store_url,table)
        generate_delete_listener(delete_url,table)
        generate_show_listener(update_url,using_ajax_get_request)
    </script>
@endpush
