@extends('layouts.admin.app')

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/css/datatables.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('custom/css/my.css') }}">
@endpush

@section('breadcrumb')
    <div class="page-header">
        <h3 class="page-title"> Role </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Setting</a></li>
            <li class="breadcrumb-item active" aria-current="page">Role</li>
        </ol>
        </nav>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <p class="card-description">
                <button type="button" class="btn btn-inverse-primary" data-toggle="modal" data-target="#modal_add"><i class="mdi mdi-plus"></i>Tambah</button>
            </p>
            <div class="template-demo">
                <x-table id="datatable">
                    @slot('head')
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Aksi</th>
                        </tr>
                    @endslot
                </x-table>
            </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@section('modal')
    <x-modal id="modal_add" header="Form Tambah">
        <x-form action="" method="post" submit="Simpan" id="form_add">
            <x-input id="add_name" name="add_name" label="Nama" placeholder="Cth. Fullstack"/>
            <x-checkbox id="add_is_developer" name="add_is_developer" label="Pengembang"/>
            <x-textarea id="add_desc" name="add_desc" label="Deskripsi" rows="5" placeholder="Cth. Adalah pengembang perangkat lunak yang memiliki kemampuan di berbagai pokok keahlian untuk membuat produk perangkat lunak"/>
        </x-form>
    </x-modal>
    <x-modal id="modal_edit" header="Form Edit">
        <x-form action="" method="put" submit="Update" id="form_edit">
            <x-input id="edit_name" name="edit_name" label="Nama" placeholder="Cth. Fullstack"/>
            <x-checkbox id="edit_is_developer" name="edit_is_developer" label="Pengembang"/>
            <x-textarea id="edit_desc" name="edit_desc" label="Deskripsi" rows="5" placeholder="Cth. Adalah pengembang perangkat lunak yang memiliki kemampuan di berbagai pokok keahlian untuk membuat produk perangkat lunak"/>
        </x-form>
    </x-modal>
@endsection

@push('js')
    <script type="text/javascript" src="{{ asset('custom/js/datatables.min.js') }}"></script>
    <script>
        let index_url = "{{ route('setting.role.index') }}"
        let store_url = "{{ route('setting.role.store') }}"
        let update_url = "{{ route('setting.role.update','-id-') }}"
        let delete_url = "{{ route('setting.role.destroy','-id-') }}"
        let using_ajax_get_request = 1

        let columns = [
            { data: 'name' }
        ]
        function show_detail(data) {
            $('#edit_name').val(data.name)
            $("#edit_is_developer").prop("checked", data.is_developer);
            $('#edit_desc').val(data.desc)
        }

        let table = generate_datatable('Role',index_url,columns)
        generate_update_listener('form_edit',table)
        generate_store_listener('form_add',store_url,table)
        generate_delete_listener(delete_url,table)
        generate_show_listener(update_url,using_ajax_get_request)
    </script>
@endpush

