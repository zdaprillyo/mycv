@extends('layouts.admin.app')

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/css/datatables.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('custom/css/my.css') }}">
    <link rel="stylesheet" href="{{ asset('custom/css/bootstrap-datetimepicker.min.css') }}">
@endpush

@section('breadcrumb')
    <div class="page-header">
        <h3 class="page-title"> Profile </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Profile</li>
        </ol>
        </nav>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <p class="card-description"></p>
            <div class="template-demo">
                <x-form action="{{ route('profile.update',1) }}" method="put" submit="Simpan" id="form_add">
                    <div class="row">
                        <div class="col">
                            <x-input id="name" name="name" label="Nama" placeholder="Cth. Aprillyo" value="{{ $profile->name }}"/>
                            <x-input id="religion" name="religion" label="Agama" placeholder="Cth. Islam" value="{{ $profile->religion }}"/>
                            <x-select id="sex" name="sex" label="Jenis Kelamin" :options="$sexs" value="{{ $profile->sex }}"/>
                            <x-select id="marital_status" name="marital_status" label="Status Perkawinan" :options="$statuses" value="{{ $profile->marital_status }}"/>
                            <div class="row">
                                <div class="col">
                                    <x-input id="birth_place" name="birth_place" label="Tempat & Tanggal Lahir" placeholder="Cth. Aprillyo" value="{{ $profile->birth_place }}"/>
                                </div>
                                <div class="col">
                                    <x-input id="birth_date" name="birth_date" type="date" label="" placeholder="Tanggal Lahir..." value="{{ toLongDate($profile->birth_date) }}"/>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <x-select id="database" name="database[]" label="Database" :options="$databases" multiple="multiple"/>
                            <x-select id="framework" name="framework[]" label="Framework" :options="$frameworks" multiple="multiple"/>
                            <x-select id="library" name="library[]" label="Library" :options="$libraries" multiple="multiple"/>
                            <x-select id="language" name="language[]" label="Language" :options="$languages" multiple="multiple"/>
                        </div>
                    </div>
                </x-form>
            </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{ asset('custom/js/moment.js') }}"></script>
    <script src="{{ asset('custom/js/id.js') }}"></script>
    <script type="text/javascript" src="{{ asset('custom/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.select-multiple').select2();
            generate_datetimepicker('.datepicker','DD MMMM YYYY')
            let selected_database = [];
            let selected_framework = [];
            let selected_library = [];
            let selected_language = [];
            @json($profile->databases).forEach(e => {
                selected_database.push(e.id)
            });
            @json($profile->frameworks).forEach(e => {
                selected_framework.push(e.id)
            });
            @json($profile->libraries).forEach(e => {
                selected_library.push(e.id)
            });
            @json($profile->languages).forEach(e => {
                selected_language.push(e.id)
            });
            $('#database').val(selected_database).trigger('change');
            $('#framework').val(selected_framework).trigger('change');
            $('#library').val(selected_library).trigger('change');
            $('#language').val(selected_language).trigger('change');
        });
    </script>
@endpush

