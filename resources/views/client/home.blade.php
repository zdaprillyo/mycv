@extends('layouts.client.app')

@section('content')
    @include('layouts.client.home.header')

    @include('layouts.client.home.services')

    @include('layouts.client.home.price-plan')

    @include('layouts.client.home.recommendation')

    @include('layouts.client.home.work')

    <!-- container -->
    <div class="container-fluid">
        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-lg-6">
                @include('layouts.client.home.education')
            </div>
            <div class="col-lg-6">
                @include('layouts.client.home.work-history')
            </div>
            <!-- col end -->
        </div>
        <!-- row end -->
    </div>
    <!-- container end -->

    @include('layouts.client.home.contact')

    @include('layouts.client.home.get-in-touch')
@endsection
