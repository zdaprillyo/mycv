@extends('layouts.admin.app')

@push('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('custom/css/datatables.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('custom/css/my.css') }}">
@endpush
@section('breadcrumb')
    <div class="page-header">
        <h3 class="page-title"> Contact </h3>
        <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Contact</li>
        </ol>
        </nav>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
            <p class="card-description">
                <button type="button" class="btn btn-inverse-primary" data-toggle="modal" data-target="#modal_add"><i class="mdi mdi-plus"></i>Tambah</button>
            </p>
            <div class="template-demo">
                <x-table id="datatable">
                    @slot('head')
                        <tr>
                            <th>No.</th>
                            <th>Foto</th>
                            <th>Nama</th>
                            <th>Tipe</th>
                            <th>Badan Hukum</th>
                            <th>Aksi</th>
                        </tr>
                    @endslot
                </x-table>
            </div>
            </div>
        </div>
        </div>
    </div>
@endsection

@section('modal')
    <x-modal id="modal_add" header="Form Tambah" size="lg">
        <x-form action="" method="post" submit="Simpan" id="form_add">
            <div class="row">
                <div class="col">
                    <x-input id="add_name" name="add_name" label="Nama" placeholder="Cth. Lio"/>
                    <x-textarea id="add_address" name="add_address" label="Alamat" placeholder="Cth. Jalan Rungkut Mejoyo Selatan"/>
                    <x-checkbox id="add_is_developer" name="add_is_developer" label="Pengembang"/>
                    <x-select id="add_legal_entity" name="add_legal_entity" label="Badan Hukum" :options="$legal_entities"/>
                </div>
                <div class="col">
                    <x-select id="add_social_medias" name="add_social_medias" label="Sosial Media" :options="$sosmeds" multiple="multiple"/>
                    <x-table id="sosmed-table">
                        @slot('head')
                            <tr>
                                <th>Nama</th>
                                <th>Akun</th>
                                <th>Link</th>
                            </tr>
                        @endslot
                        @slot('body')
                        @endslot
                    </x-table>
                </div>
            </div>

        </x-form>
    </x-modal>
    <x-modal id="modal_edit" header="Form Edit" size="lg">
        <x-form action="" method="put" submit="Update" id="form_edit">
            <div class="row">
                <div class="col">
                    <x-input id="edit_name" name="edit_name" label="Nama" placeholder="Cth. Lio"/>
                    <x-textarea id="edit_address" name="edit_address" label="Alamat" placeholder="Cth. Jalan Rungkut Mejoyo Selatan"/>
                    <x-checkbox id="edit_is_developer" name="edit_is_developer" label="Pengembang"/>
                    <x-select id="edit_legal_entity" name="edit_legal_entity" label="Badan Hukum" :options="$legal_entities"/>
                </div>
                <div class="col">
                    <x-select id="edit_social_medias" name="edit_social_medias" label="Sosial Media" :options="$sosmeds" multiple="multiple"/>
                    <x-table id="edit-sosmed-table">
                        @slot('head')
                            <tr>
                                <th>Nama</th>
                                <th>Akun</th>
                                <th>Link</th>
                            </tr>
                        @endslot
                        @slot('body')
                        @endslot
                    </x-table>
                </div>
            </div>
        </x-form>
    </x-modal>
@endsection

@push('js')
    <script type="text/javascript" src="{{ asset('custom/js/datatables.min.js') }}"></script>
    <script>
        let index_url = "{{ route('contact.index') }}"
        let store_url = "{{ route('contact.store') }}"
        let update_url = "{{ route('contact.update','-id-') }}"
        let delete_url = "{{ route('contact.destroy','-id-') }}"
        let using_ajax_get_request = 1
        $('.select-multiple').select2();

        var sosmed_name = @json($sosmeds);
        var sosmed_selected = []

        $('#add_social_medias,#edit_social_medias').on('select2:select', function (e) {
            $(this).val().forEach(e => {
              if(!sosmed_selected.find( x => x.id == e)){
                sosmed_selected.push({ id : e,name:sosmed_name[e] ?? '-', akun : '', link : '' })
              }
            });
            let action = 1
            if(e.currentTarget.id == 'edit_social_medias'){
                action = 2
            }
            draw_on_table(sosmed_selected,action)
        });

        $('#add_social_medias,#edit_social_medias').on('select2:unselecting', function (e) {
            let unselected_id = e.params.args.data.id
            sosmed_selected = sosmed_selected.filter(function(e, index, arr){
                return e.id != unselected_id;
            });
            let action = 1
            if(e.currentTarget.id == 'edit_social_medias'){
                action = 2
            }
            draw_on_table(sosmed_selected,action)
        });

        function changeInfo(info,id){
            if(info==1){
                sosmed_selected.find( x => x.id == id).akun = $('#akun_'+id).val()
            }else if(info==2){
                sosmed_selected.find( x => x.id == id).link = $('#link_'+id).val()
            }
        }

        function draw_on_table(params,action = 1){ // action: 1, create|2, update
            let html = ''
            params.forEach(e => {
                let akun_id = (action == 1 ? 'akun_' : 'edit_akun_') + e.id
                let link_id = (action == 1 ? 'link_' : 'edit_link_') + e.id
                let akun_name = action == 1 ? 'akun[]' : 'edit_akun[]'
                let link_name = action == 1 ? 'link[]' : 'edit_link[]'
                let main_id = action == 1 ? 'sosmeds[]' : 'edit_sosmeds[]'
                html+='<input type=\"hidden\" name="'+main_id+'" value=\"'+e.id+'\" ></td>'
                html+='<tr><td>'+e.name+'</td><td><input id="'+akun_id+'" name="'+akun_name+'" type=\"text\" class=\"form-control\" value=\"'+e.akun+'\" placeholder=\"Akun...\" onChange="changeInfo(1,'+e.id+')"></td>'
                html+='<td><input type=\"text\" id="'+link_id+'" name="'+link_name+'" class=\"form-control\" value=\"'+e.link+'\" placeholder=\"Link...\" onChange="changeInfo(2,'+e.id+')"></td></tr>'
            });
            if(action == 1){
                $('#sosmed-table tbody').html(html)
            }else{
                $('#edit-sosmed-table tbody').html(html)
            }
        }

        let columns = [
            { data: 'photo', searchable: false, orderable: false },
            { data: 'name' },
            { data: 'is_developer' },
            { data: 'legal_entity' },
        ]
        function show_detail(data) {
            $('#edit_name').val(data.name)
            $('#edit_address').val(data.address)
            $("#edit_is_developer").prop("checked", data.is_developer);
            $('#edit_legal_entity').val(data.legal_entity)
            let temp = []
            sosmed_selected = []
            data.sosmed.forEach(e => {
                temp.push(e.id)
                sosmed_selected.push({ id : e.id,name:sosmed_name[e.id] ?? '-', akun : e.account, link : e.link })
            });
            $('#edit_social_medias').val(temp).trigger('change');
            draw_on_table(sosmed_selected,2)
        }

        let table = generate_datatable('Contact',index_url,columns)
        generate_update_listener('form_edit',table)
        generate_store_listener('form_add',store_url,table)
        generate_delete_listener(delete_url,table)
        generate_show_listener(update_url,using_ajax_get_request)
    </script>
@endpush
