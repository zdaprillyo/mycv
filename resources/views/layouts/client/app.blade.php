<!doctype html>
<html lang="zxx">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- color of address bar in mobile browser -->
  <meta name="theme-color" content="#2B2B35">
  <!-- favicon  -->
  <link rel="shortcut icon" href="{{ asset('client/img/thumbnail.ico') }}" type="image/x-icon">
  <!-- bootstrap css -->
  <link rel="stylesheet" href="{{ asset('client/css/plugins/bootstrap.min.css') }}">
  <!-- font awesome css -->
  <link rel="stylesheet" href="{{ asset('client/css/plugins/font-awesome.min.css') }}">
  <!-- swiper css -->
  <link rel="stylesheet" href="{{ asset('client/css/plugins/swiper.min.css') }}">
  <!-- fancybox css -->
  <link rel="stylesheet" href="{{ asset('client/css/plugins/fancybox.min.css') }}">
  <!-- main css -->
  <link rel="stylesheet" href="{{ asset('client/css/style.css') }}">

  <title>Arter onepage</title>
</head>

<body>

  <!-- app -->
  <div class="art-app art-app-onepage">
    <!-- mobile top bar -->
    <div class="art-mobile-top-bar"></div>
    <!-- app wrapper -->
    <div class="art-app-wrapper">
      <!-- app container end -->
      <div class="art-app-container">
        <!-- info/side bar -->
        @include('layouts.client.sidebar')
        <!-- info/side bar end -->
        <!-- content -->
        <div class="art-content">
            <!-- curtain -->
            <div class="art-curtain"></div>
            <!-- top background -->
            <div class="art-top-bg" style="background-image: url(img/bg.jpg)">
                <!-- overlay -->
                <div class="art-top-bg-overlay"></div>
                <!-- overlay end -->
            </div>
            <!-- top background end -->

            <!-- swup container -->
            <div class="transition-fade" id="swup">
                <!-- scroll frame -->
                <div id="scrollbar" class="art-scroll-frame">
                    @yield('content')
                    <!-- container -->
                    <div class="container-fluid">
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-6 col-lg-3">
                                <!-- brand -->
                                <img class="art-brand" src="img/brands/1.png" alt="brand">
                            </div>
                            <!-- col end -->

                            <!-- col -->
                            <div class="col-6 col-lg-3">
                                <!-- brand -->
                                <img class="art-brand" src="img/brands/2.png" alt="brand">
                            </div>
                            <!-- col end -->

                            <!-- col -->
                            <div class="col-6 col-lg-3">
                                <!-- brand -->
                                <img class="art-brand" src="img/brands/3.png" alt="brand">
                            </div>
                            <!-- col end -->

                            <!-- col -->
                            <div class="col-6 col-lg-3">
                                <!-- brand -->
                                <img class="art-brand" src="img/brands/1.png" alt="brand">
                            </div>
                            <!-- col end -->
                        </div>
                        <!-- row end -->
                    </div>
                    <!-- container end -->
                    <!-- container -->
                    <div class="container-fluid">
                        <!-- footer -->
                        <footer>
                            <!-- copyright -->
                            <div>© 2020 Artur Carter</div>
                            <!-- author ( Please! Do not delete it. You are awesome! :) -->
                            <div>Template author:&#160; <a href="https://themeforest.net/user/millerdigitaldesign" target="_blank">Nazar Miller</a></div>
                        </footer>
                        <!-- footer end -->
                    </div>
                    <!-- container end -->
                </div>
                <!-- scroll frame end -->
            </div>
            <!-- swup container end -->
        </div>
        <!-- content end -->
      </div>
      <!-- app container end -->
    </div>
    <!-- app wrapper end -->

    <!-- preloader -->
    <div class="art-preloader">
      <!-- preloader content -->
      <div class="art-preloader-content">
        <!-- title -->
        <h4>Artur Carter</h4>
        <!-- progressbar -->
        <div id="preloader" class="art-preloader-load"></div>
      </div>
      <!-- preloader content end -->
    </div>
    <!-- preloader end -->
  </div>
  <!-- app end -->
  <div id="swupMenu"></div>

  <!-- jquery js -->
  <script src="{{ asset('client/js/plugins/jquery.min.js') }}"></script>
  <!-- anime js -->
  <script src="{{ asset('client/js/plugins/anime.min.js') }}"></script>
  <!-- swiper js -->
  <script src="{{ asset('client/js/plugins/swiper.min.js') }}"></script>
  <!-- progressbar js -->
  <script src="{{ asset('client/js/plugins/progressbar.min.js') }}"></script>
  <!-- smooth scrollbar js -->
  <script src="{{ asset('client/js/plugins/smooth-scrollbar.min.js') }}"></script>
  <!-- overscroll js -->
  <script src="{{ asset('client/js/plugins/overscroll.min.js') }}"></script>
  <!-- typing js -->
  <script src="{{ asset('client/js/plugins/typing.min.js') }}"></script>
  <!-- isotope js -->
  <script src="{{ asset('client/js/plugins/isotope.min.js') }}"></script>
  <!-- fancybox js -->
  <script src="{{ asset('client/js/plugins/fancybox.min.js') }}"></script>
  <!-- swup js -->
  <script src="{{ asset('client/js/plugins/swup.min.js') }}"></script>

  <!-- main js -->
  <script src="{{ asset('client/js/main.js') }}"></script>

</body>

</html>
