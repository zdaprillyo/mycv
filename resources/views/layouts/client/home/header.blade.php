<!-- container -->
<div class="container-fluid">
    <!-- row -->
    <div class="row p-60-0 p-lg-30-0 p-md-15-0">
        <!-- col -->
        <div class="col-lg-12">
            <!-- banner -->
            <div class="art-a art-banner" style="background-image: url(img/bg.jpg)">
                <!-- banner back -->
                <div class="art-banner-back"></div>
                <!-- banner dec -->
                <div class="art-banner-dec"></div>
                <!-- banner overlay -->
                <div class="art-banner-overlay">
                    <!-- main title -->
                    <div class="art-banner-title">
                        <!-- title -->
                        <h1 class="mb-15">Discover my Amazing <br>Art Space!</h1>
                        <!-- suptitle -->
                        <div class="art-lg-text art-code mb-25">&lt;<i>code</i>&gt; I build <span class="txt-rotate" data-period="2000"
                            data-rotate='[ "web interfaces.", "ios and android applications.", "design mocups.", "automation tools." ]'></span>&lt;/<i>code</i>&gt;</div>
                        <div class="art-buttons-frame">
                            <!-- button -->
                            <a href="#." class="art-btn art-btn-md"><span>Explore now</span></a>
                            <!-- button -->
                            <a href="#." class="art-link art-white-link art-w-chevron">Hire me</a>
                        </div>
                    </div>
                    <!-- main title end -->
                    <!-- photo -->
                    <img src="img/face-2.png" class="art-banner-photo" alt="Your Name">
                </div>
                <!-- banner overlay end -->
            </div>
            <!-- banner end -->
        </div>
        <!-- col end -->
    </div>
    <!-- row end -->
</div>
<!-- container end -->

<!-- container -->
<div class="container-fluid">
    <!-- row -->
    <div class="row p-30-0">
        <!-- col -->
        <div class="col-md-3 col-6">
            <!-- couner frame -->
            <div class="art-counter-frame">
                <!-- counter -->
                <div class="art-counter-box">
                    <!-- counter number -->
                    <span class="art-counter">10</span><span class="art-counter-plus">+</span>
                </div>
                <!-- counter end -->
                <!-- title -->
                <h6>Years Experience</h6>
            </div>
            <!-- couner frame end -->
        </div>
        <!-- col end -->

        <!-- col -->
        <div class="col-md-3 col-6">
            <!-- couner frame -->
            <div class="art-counter-frame">
                <!-- counter -->
                <div class="art-counter-box">
                    <!-- counter number -->
                    <span class="art-counter">143</span>
                </div>
                <!-- counter end -->
                <!-- title -->
                <h6>Completed Projects</h6>
            </div>
            <!-- couner frame end -->
        </div>
        <!-- col end -->

        <!-- col -->
        <div class="col-md-3 col-6">
            <!-- couner frame -->
            <div class="art-counter-frame">
                <!-- counter -->
                <div class="art-counter-box">
                    <!-- counter number -->
                    <span class="art-counter">114</span>
                </div>
                <!-- counter end -->
                <!-- title -->
                <h6>Happy Customers</h6>
            </div>
            <!-- couner frame end -->
        </div>
        <!-- col end -->

        <!-- col -->
        <div class="col-md-3 col-6">
            <!-- couner frame -->
            <div class="art-counter-frame">
                <!-- counter -->
                <div class="art-counter-box">
                    <!-- counter number -->
                    <span class="art-counter">20</span><span class="art-counter-plus">+</span>
                </div>
                <!-- counter end -->
                <!-- title -->
                <h6>Honors and Awards</h6>
            </div>
            <!-- couner frame end -->
        </div>
        <!-- col end -->
    </div>
    <!-- row end -->
</div>
<!-- container end -->
