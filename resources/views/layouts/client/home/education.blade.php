            <!-- section title -->
            <div class="art-section-title">
                <!-- title frame -->
                <div class="art-title-frame">
                <!-- title -->
                <h4>Education</h4>
                </div>
                <!-- title frame end -->
            </div>
            <!-- section title end -->

            <!-- timeline -->
            <div class="art-timeline art-gallery" id="history">
                <div class="art-timeline-item">
                <div class="art-timeline-mark-light"></div>
                <div class="art-timeline-mark"></div>

                <div class="art-a art-timeline-content">
                    <div class="art-card-header">
                    <div class="art-left-side">
                        <h5>Title of section 1</h5>
                        <div class="art-el-suptitle mb-15">Template author</div>
                    </div>
                    <div class="art-right-side">
                        <span class="art-date">jan 2018 - may 2020</span>
                    </div>
                    </div>

                    <p>Dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut.</p>
                    <a data-fancybox="diplome" href="files/certificate.jpg" class="art-link art-color-link art-w-chevron">Diplome</a>
                </div>
                </div>

                <div class="art-timeline-item">
                <div class="art-timeline-mark-light"></div>
                <div class="art-timeline-mark"></div>

                <div class="art-a art-timeline-content">
                    <div class="art-card-header">
                    <div class="art-left-side">
                        <h5>Title of section 1</h5>
                        <div class="art-el-suptitle mb-15">Template author</div>
                    </div>
                    <div class="art-right-side">
                        <span class="art-date">jan 2018 - may 2020</span>
                    </div>
                    </div>
                    <div>Consectetur adipisicing elit. Iusto, optio, dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde?</div>
                </div>
                </div>

                <div class="art-timeline-item">
                <div class="art-timeline-mark-light"></div>
                <div class="art-timeline-mark"></div>

                <div class="art-a art-timeline-content">
                    <div class="art-card-header">
                    <div class="art-left-side">
                        <h5>Title of section 1</h5>
                        <div class="art-el-suptitle mb-15">Template author</div>
                    </div>
                    <div class="art-right-side">
                        <span class="art-date">jan 2018 - may 2020</span>
                    </div>
                    </div>
                    <p>Dolorum provident rerum aut hic quasi placeat iure tempora laudantium ipsa ad debitis unde? Iste voluptatibus minus veritatis qui ut.</p>
                    <a data-fancybox="diplome" href="files/certificate.jpg" class="art-link art-color-link art-w-chevron">Licence</a>
                </div>

                </div>

                <div class="art-timeline-item">
                <div class="art-timeline-mark-light"></div>
                <div class="art-timeline-mark"></div>

                <div class="art-a art-timeline-content">
                    <div class="art-card-header">
                    <div class="art-left-side">
                        <h5>Title of section 1</h5>
                        <div class="art-el-suptitle mb-15">Template author</div>
                    </div>
                    <div class="art-right-side">
                        <span class="art-date">jan 2018 - may 2020</span>
                    </div>
                    </div>
                    <p>Ipsum dolor sit amet, consectetur adipisicing elit. Iusto, optio, dolorum provident rerum.</p>
                    <a data-fancybox="diplome" href="files/certificate.jpg" class="art-link art-color-link art-w-chevron">Certificate</a>
                </div>

                </div>

            </div>
            <!-- timeline end -->
