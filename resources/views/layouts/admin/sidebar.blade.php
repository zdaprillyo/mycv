<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <div class="sidebar-brand-wrapper d-none d-lg-flex align-items-center justify-content-center fixed-top">
      <a class="sidebar-brand brand-logo" href="{{ route('dashboard.index') }}"><img src="{{ asset('assets/images/logo.svg') }}" alt="logo" /></a>
      <a class="sidebar-brand brand-logo-mini" href="{{ route('dashboard.index') }}"><img src="{{ asset('assets/images/logo-mini.svg') }}" alt="logo" /></a>
    </div>
    <ul class="nav">
      <li class="nav-item nav-category">
        <span class="nav-link">Menu</span>
      </li>
      <li class="nav-item menu-items">
        <a class="nav-link" href="{{ route('dashboard.index') }}">
          <span class="menu-icon">
            <i class="mdi mdi-speedometer"></i>
          </span>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>
      <li class="nav-item menu-items">
        <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
          <span class="menu-icon">
            <i class="mdi mdi-laptop"></i>
          </span>
          <span class="menu-title">Setting</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="ui-basic">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{ route('setting.database.index') }}">Database</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('setting.language.index') }}">Language</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('setting.religion.index') }}">Religion</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('setting.social-media.index') }}">Social Media</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('setting.work-type.index') }}">Work Type</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('setting.framework.index') }}">Framework</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('setting.library.index') }}">Library</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('setting.role.index') }}">Role</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('setting.educational-level.index') }}">Educational Level</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('setting.service.index') }}">Service</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('setting.tool.index') }}">Tool</a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item menu-items">
        <a class="nav-link" href="{{ route('contact.index') }}">
          <span class="menu-icon">
            <i class="mdi mdi-contact-mail"></i>
          </span>
          <span class="menu-title">Contact</span>
        </a>
      </li>
      <li class="nav-item menu-items">
        <a class="nav-link" href="{{ route('project.index') }}">
          <span class="menu-icon">
            <i class="mdi mdi-playlist-play"></i>
          </span>
          <span class="menu-title">Project</span>
        </a>
      </li>
      <li class="nav-item menu-items">
        <a class="nav-link" href="{{ route('work-experience.index') }}">
          <span class="menu-icon">
            <i class="mdi mdi-table-large"></i>
          </span>
          <span class="menu-title">Work Experience</span>
        </a>
      </li>
      <li class="nav-item menu-items">
        <a class="nav-link" href="{{ route('education-record.index') }}">
          <span class="menu-icon">
            <i class="mdi mdi-chart-bar"></i>
          </span>
          <span class="menu-title">Education Record</span>
        </a>
      </li>
    </ul>
</nav>


